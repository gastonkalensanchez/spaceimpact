using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlienTierra_Correr : EnemigoEstadoBase
{
    MoverEnemigos m_e;
    public AlienTierra_Correr(MoverEnemigos _m_e):base("AlienTierraCorre",_m_e)
    {   
        m_e = (MoverEnemigos)_m_e;
    }

    public float amplitudey = 2f; // Amplitud del movimiento en arcoíris
    public float amplitudex = 9f;
    public float frequency = 2f; // Frecuencia del movimiento en arcoíris
    public float velocidad = 20f;
    private Vector3 initialPosition;
    public float distancia = 220f;
    public Vector3 nuevapos;
    public float PosIn;
    public override void Inicio()
    {
        base.Inicio();
        amplitudex = Random.Range(8f,13f);
        frequency = Random.Range(2f,3f);
        m_e.PuedeMorir = false;
        m_e.auxmuertelvl2 = true;
    }
    public override void Actualizar()
    {
        base.Actualizar();
        MovimientoZicZac();
    }
    public override void Terminar()
    {
        base.Terminar();
    }
    void MovimientoZicZac()
    {
        if (m_e.transform.childCount == 0)
        {
            m_e.PuedeMorir = true;
            m_e.RecibiDaño();
        }
        // Calcula la dirección hacia el jugador en el eje z
        float time = Time.time;
        float xOffset = Mathf.Sin(time * frequency) * amplitudex;
        float yOffset = Mathf.Cos(time * frequency) * amplitudey;
        nuevapos = new Vector3((PosIn + xOffset), yOffset , m_e.transform.position.z - 20f * Time.deltaTime);    
        m_e.transform.position = nuevapos;
    }


    
}
