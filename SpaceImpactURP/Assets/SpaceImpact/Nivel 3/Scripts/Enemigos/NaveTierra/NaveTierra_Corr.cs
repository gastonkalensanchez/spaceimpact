
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NaveTierra_Corr : EnemigoEstadoBase
{
    MoverEnemigos m_e;
    public int punto;
    public bool _Disparo;
    public NaveTierra_Corr(MoverEnemigos _m_e) : base("NaveTierraCorre", _m_e)
    {
        m_e = (MoverEnemigos)_m_e;
    }
    public override void Inicio()
    {
        base.Inicio();
        m_e.transform.position = new Vector3(m_e.transform.position.x, 0, Random.Range(30f, 100f));
        punto = 0;

    }
    public override void Actualizar()
    {
        base.Actualizar();
        if (m_e.NaveTierra == null)
        {
            m_e.PuedeMorir = true;
            m_e.RecibiDaņo();
        }
        if (punto >= m_e.NaveTierraPuntos.Length)
        {
            punto = 0;
        }

        var targetPosition = m_e.NaveTierraPuntos[punto].position;
        if (m_e.NaveTierra.position == targetPosition && m_e.NaveTierra != null)
        {
            punto++;
            m_e.Disparar(m_e.PuntoDeDisparoNaveTierra);
        }
        else
        {
            MoverHaciaLaPos(punto);
        }


    }
    public override void Terminar()
    {
        base.Terminar();
    }

    public void MoverHaciaLaPos(int pPos)
    {

        m_e.NaveTierra.GetComponent<Rigidbody>().MovePosition(Vector3.MoveTowards(m_e.NaveTierra.position, m_e.NaveTierraPuntos[punto].position, 5 * Time.deltaTime));

    }


}