using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JefeLvl3 : MonoBehaviour
{

    public float amplitudey; // Amplitud del movimiento en arco�ris
    public float amplitudex;
    public float frequency; // Frecuencia del movimiento en arco�ris
    public float velocidad;
    public float distancia;
    private Vector3 initialPosition;
    public Vector3 nuevapos;
    public float PosIn;

    public Jefe _jefecod;
    public bool PuedeDisparar;
    public Transform puntodisparo;
    public float auxdisparo;
    // Start is called before the first frame update
    void Start()
    {
        initialPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        float FILLBOSS = _jefecod.Naves.Count / (float)6;
        _jefecod.Canvas.GetComponent<UI>().BossLife.value = FILLBOSS;
        puntodisparo.LookAt(_jefecod._Jugador.transform.position);
        auxdisparo += Time.deltaTime;
        if (auxdisparo >= Random.Range(0.5f,1) && PuedeDisparar)
        {
            Disparar(puntodisparo);
            auxdisparo = 0;
        }
        float time = Time.time;
        float xOffset = Mathf.Sin(time * frequency) * amplitudex;
        float yOffset = Mathf.Cos(time * frequency) * amplitudey;
        float distance = Mathf.PingPong(time * velocidad, distancia) - 1;
        nuevapos = new Vector3((PosIn + xOffset), yOffset + 2, initialPosition.z - distance);

        transform.position = nuevapos;
    }
    public void Disparar(Transform pPuntoDeDisparo)
    {

        GameObject p = Instantiate(_jefecod._Proyectil, pPuntoDeDisparo.transform.position, pPuntoDeDisparo.rotation);
        p.transform.localScale = new Vector3(60,60,60);
        p.GetComponent<Rigidbody>().AddForce(pPuntoDeDisparo.transform.forward * 50, ForceMode.Impulse);
        Destroy(p, 5f);


    }
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Proyectil"))
        {
            
            GameObject par = Instantiate(_jefecod.Particulas, transform.position, Quaternion.identity);
            par.transform.localScale = new Vector3(3, 3, 3);

            Destroy(other.gameObject);
            Destroy(gameObject);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Proyectil"))
        {
            GameObject par = Instantiate(_jefecod.Particulas, transform.position, Quaternion.identity);
            par.transform.localScale = new Vector3(3, 3, 3);

            Destroy(other.gameObject);
            Destroy(gameObject);
        }
    }
}
