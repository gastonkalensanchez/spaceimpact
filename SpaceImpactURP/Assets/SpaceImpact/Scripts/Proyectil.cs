using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Proyectil : MonoBehaviour
{
    internal int Daño;
    void Start()
    {
        if (CompareTag("Proyectil"))
            Daño = 10;

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("DesIns"))
        {
            Destroy(gameObject);
        }
        if (other.gameObject.CompareTag("EnemigoBala"))
        {
            Destroy(other.gameObject);
        }
       

    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("DesIns"))
        {
            Destroy(gameObject);
        }
    }

}
