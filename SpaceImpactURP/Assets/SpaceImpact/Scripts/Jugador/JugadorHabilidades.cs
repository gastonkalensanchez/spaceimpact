using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    public enum TipoDeHabilidad 
    {
        Escudo,Cohetes,Evacion,Aliados,PotenciarBala,Ataque,Bolas,Inmortal,
        Rafaga,Dash,RegenerarVida,PasivaCohete,Revivir,LuegoDeUnaHabilidad,
        VelocidadDeAtaque
    
    }
public class JugadorHabilidades
{
    

    public List<TipoDeHabilidad> habilidadesdesbloqueadas;
    public JugadorHabilidades()
    {
        habilidadesdesbloqueadas = new List<TipoDeHabilidad>();
    }
    public void DesbloquearHabilidad(TipoDeHabilidad tipodehabilidad)
    {
     
        if (!habilidadesdesbloqueadas.Contains(tipodehabilidad))
        {
            habilidadesdesbloqueadas.Add(tipodehabilidad);
        }
        
    
    }
    public void SacarHabilidad(TipoDeHabilidad tipodehabilidad)
    {
        habilidadesdesbloqueadas.Remove(tipodehabilidad);
    }
    public bool ContieneLaHabilidad(TipoDeHabilidad tipoDeHabilidad)
    {
        if (habilidadesdesbloqueadas.Contains(tipoDeHabilidad))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public bool EstaDebloqueadaLaHabilidad(TipoDeHabilidad tipodehabilidad)
    {
        return habilidadesdesbloqueadas.Contains(tipodehabilidad);
    }
    public void EliminarTodasLasHabilidadesCargadas()
    {
        habilidadesdesbloqueadas.Clear();
    }




   
}
