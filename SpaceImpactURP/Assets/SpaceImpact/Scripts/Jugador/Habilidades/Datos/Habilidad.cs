using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Newtonsoft.Json;
[CreateAssetMenu(fileName = "Habilidad")]

[System.Serializable]
public class Habilidad : ScriptableObject
{

    public string imagenID;
    [JsonIgnore]
    public Sprite imagen;
    public enum NombreDeLaHabilidad
    {

        Escudo,Cohetes,Evacion,Aliados,PotenciarBala,Ataque,Bolas,Inmortal,Dash,RegenerarVida,PasivaCohete,Revivir,LuegoDeUnaHabilidad

    }
    public NombreDeLaHabilidad nombre;
    
    public string DescripcionHabilidad;

    public int Costo;
    public enum TipoDeHabilidad {Pasiva,Activa}
    public TipoDeHabilidad tipodehabilidad;

    public float actualenfriamiento;
    public float maximoenfriamiento;

    public KeyCode tecla;
    public void PonerEnEnfriamiento()
    {
        EnfriamientoManager.intancia.EmpezarEnfriamiento(this);
    }
    public bool HabilidadLista()
    {
        if (actualenfriamiento <= 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
