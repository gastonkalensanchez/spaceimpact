using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class Habilidades : MonoBehaviour
{
    public Habilidad habilidad;
    public Image Imagen;
    public TMP_Text Nombre;
    public TMP_Text Descripcion;
    public TMP_Text Tipo;
    public TMP_Text Enfriamineto;
    public GameObject Aprender;
    public bool descripcion;

    void Start()
    {

 
    }
    private void Update()
    {
        CargarDatos();
    }
    void CargarDatos()
    {
        if (habilidad != null)
        {
            if (Imagen != null)
            {
                foreach (var item in ArbolHabilidadesSI._ArbolHabilidadesSI.imagenes)
                {
                    
                    if (item.name == habilidad.imagenID)
                    {
                        Debug.Log(item.name);
                        Imagen.sprite = item;
                    }
                }
                
            }
            Nombre.text = habilidad.nombre.ToString();
            Descripcion.text = habilidad.DescripcionHabilidad;
            Tipo.text = habilidad.tipodehabilidad.ToString();
            Enfriamineto.text = habilidad.maximoenfriamiento.ToString();
            if (descripcion)
            {
                Aprender.GetComponent<Button>().onClick.AddListener(delegate { ArbolHabilidadesSI._ArbolHabilidadesSI.DesbloquearHabilidad(habilidad); });
            }
            
        }

    }
}
