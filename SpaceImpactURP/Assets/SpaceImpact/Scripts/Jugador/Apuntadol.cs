using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Apuntadol : MonoBehaviour
{
    public float rotationSpeed = 50f;  
    private float maxRotationY = 9f;  
    private float currentRotationY = 0f;
    public Jugador j;
    
    void Update()
    {

        if (!j.Murio)
        {
            float rotationInput = Input.GetAxis("Horizontal");
            float rotationAmount = rotationInput * rotationSpeed * Time.deltaTime;

            // Calcula la nueva rotación
            currentRotationY += rotationAmount;

            // Restringe la rotación dentro del rango de -maxRotationY a maxRotationY
            currentRotationY = Mathf.Clamp(currentRotationY, -maxRotationY, maxRotationY);

            float playerPosX = transform.position.x;
            float cameraPosX = Camera.main.transform.position.x;
            float leftLimit = cameraPosX - 1f;
            float rightLimit = cameraPosX + 1f;
            if (playerPosX > leftLimit && playerPosX < rightLimit)
            {
                currentRotationY = 0f;
            }
            transform.rotation = Quaternion.Euler(0f, currentRotationY, 0f);

        }


    }
}
