using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
public class VerHabilidadesCargadas : MonoBehaviour
{
    public List<GameObject> Habilidades = new List<GameObject>(); 
    public int CantidadHabilidades;
    public int auxCantHabilidad;

    void Start()
    {
        
    }

    
    void Update()
    {
        
        VerMisHabilidades();
        
    }


    void VerMisHabilidades()
    {
        CantidadHabilidades = ArbolHabilidadesSI._ArbolHabilidadesSI.habilidadescargadas.Count;
        int index = 0; // Variable local para almacenar el índice actual

        foreach (var item in Habilidades)
        {
            if (CantidadHabilidades <= auxCantHabilidad)
            {
                item.SetActive(false);
            }
            else
            {
                int currentIndex = index;
                item.SetActive(true);
                foreach (var im in ArbolHabilidadesSI._ArbolHabilidadesSI.imagenes)
                {
                    if (im.name == ArbolHabilidadesSI._ArbolHabilidadesSI.habilidadescargadas[currentIndex].imagenID)
                    {
                        item.GetComponent<Image>().sprite = im;
                    }
                }
                
                item.GetComponent<Button>().onClick.AddListener(() => ArbolHabilidadesSI._ArbolHabilidadesSI.descripcionH.Invoke(currentIndex,ArbolHabilidadesSI._ArbolHabilidadesSI.habilidadescargadas));
            }

            auxCantHabilidad++;
            index++; 
        }

        auxCantHabilidad = 0;
    }


}
