using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System.Linq;


public class ArbolHabilidadesSI : MonoBehaviour
{
    public List<Sprite> imagenes;
    public static ArbolHabilidadesSI _ArbolHabilidadesSI;
    public JugadorHabilidades _jugadorhabilidades;
    public List<Button> HabilidadesParaEleguir = new List<Button>();
    public List<Habilidad> todaslashabilidades = new List<Habilidad>();
    public List<Habilidad> habilidadescargadas = new List<Habilidad>();
    public List<Habilidad> habilidadesEnMano = new List<Habilidad>();
    public GameObject DescripcionHabilidades;
    int H1, H2, H3;
    public List<int> availableNumbers = new List<int>();
    public delegate void DescripcionH(int botonIndex, List<Habilidad> plisthabilidad);
    public DescripcionH descripcionH;
    public delegate void BotonPresionadoDelegate(Habilidad phabilidad);
    public BotonPresionadoDelegate botonPresionadoDelegate;
    public GameObject _TodasLasHabilidades;
    public GameObject _DesbloquearHabilidad;

    void Awake()
    {
        if (_ArbolHabilidadesSI == null)
        {
            _ArbolHabilidadesSI = this;
        }

    }
    void Start()
    {
        HabilidadesManager._Persistencia.CargarHabilidades();

        habilidadescargadas.RemoveAll(item => item == null);
        todaslashabilidades.RemoveAll(item => habilidadescargadas.Contains(item));
        RestablecerEnfriamientoYTeclas();
        _jugadorhabilidades = GetComponent<Jugador>().GetJugadorHabilidades();

        foreach (var item in ArbolHabilidadesSI._ArbolHabilidadesSI.todaslashabilidades)
        {
            item.tecla = KeyCode.None;
            item.actualenfriamiento = 0;
        }
        botonPresionadoDelegate = DesbloquearHabilidad;
        descripcionH = DescripcionHabilidad;




    }

    void Update()
    {
        //elimina el duplicado de las habilidades
        habilidadescargadas = habilidadescargadas
        .GroupBy(h => h.nombre)
        .Select(g => g.First())
        .ToList();
        habilidadescargadas.RemoveAll(item => item == null);
        todaslashabilidades.RemoveAll(h => habilidadescargadas.Any(hc => hc.nombre == h.nombre));
        CargarTeclasHabilidadesMano();

    }

    public void MostrarHabilidadesParaEleguir()
    {
        CargarRandomNumber();
        H1 = GetRandomNumber();
        H2 = GetRandomNumber();
        H3 = GetRandomNumber();

        List<Habilidad> habilidadesDisponibles = todaslashabilidades; // Copiar las habilidades disponibles

        if (habilidadesDisponibles.Count >= 1)
        {
            HabilidadesParaEleguir[0].interactable = true;
            HabilidadesParaEleguir[0].GetComponent<Habilidades>().habilidad = habilidadesDisponibles[H1];
            HabilidadesParaEleguir[0].onClick.RemoveAllListeners();
            HabilidadesParaEleguir[0].onClick.AddListener(() => botonPresionadoDelegate.Invoke(habilidadesDisponibles[H1]));
        }
        else
        {
            HabilidadesParaEleguir[0].gameObject.SetActive(false);
            AtrasArbolDeJugador();
        }

        if (habilidadesDisponibles.Count >= 2)
        {
            HabilidadesParaEleguir[1].interactable = true;
            HabilidadesParaEleguir[1].GetComponent<Habilidades>().habilidad = habilidadesDisponibles[H2];
            HabilidadesParaEleguir[1].onClick.RemoveAllListeners();
            HabilidadesParaEleguir[1].onClick.AddListener(() => botonPresionadoDelegate.Invoke(habilidadesDisponibles[H2]));
        }
        else
        {
            HabilidadesParaEleguir[1].gameObject.SetActive(false);
        }

        if (habilidadesDisponibles.Count >= 3)
        {
            HabilidadesParaEleguir[2].interactable = true;
            HabilidadesParaEleguir[2].GetComponent<Habilidades>().habilidad = habilidadesDisponibles[H3];
            HabilidadesParaEleguir[2].onClick.RemoveAllListeners();
            HabilidadesParaEleguir[2].onClick.AddListener(() => botonPresionadoDelegate.Invoke(habilidadesDisponibles[H3]));
        }
        else
        {
            HabilidadesParaEleguir[2].gameObject.SetActive(false);
        }
        
    }

    private void CargarRandomNumber()
    {
        availableNumbers.Clear();
        for (int i = 0; i < todaslashabilidades.Count; i++)
        {
            availableNumbers.Add(i);
        }
    }
    private int GetRandomNumber()
    {

        // Verificar si no hay más números disponibles
        if (availableNumbers.Count == 0)
        {
            Debug.LogWarning("Ya no hay más números disponibles.");
            return 0; // Puedes manejar este caso de acuerdo a tus necesidades
        }

        // Generar un índice aleatorio dentro del rango de la lista
        int randomIndex = UnityEngine.Random.Range(0, availableNumbers.Count);

        // Obtener el número en el índice aleatorio
        int randomNumber = availableNumbers[randomIndex];

        // Remover el número de la lista para evitar repeticiones
        availableNumbers.RemoveAt(randomIndex);

        return randomNumber;
    }

    public void DescripcionHabilidad(int botonIndex, List<Habilidad> plisthabilidad)
    {
        DescripcionHabilidades.SetActive(true);
        if (botonIndex >= 0 && botonIndex < plisthabilidad.Count)
        {
            DescripcionHabilidades.GetComponent<Habilidades>().habilidad = plisthabilidad[botonIndex];
        }
    }
    public void AtrasDescripcionHabilidades()
    {
        DescripcionHabilidades.SetActive(false);
    }
    public void AtrasArbolDeJugador()
    {
        DescripcionHabilidades.SetActive(false);
        _TodasLasHabilidades.SetActive(false);
        _DesbloquearHabilidad.SetActive(false);
        GameControllerSI._gameControllerLvL1.PausarJuego = false;
    }


    public void DesbloquearHabilidad(Habilidad pHabilidad)
    {
        foreach (var item in HabilidadesParaEleguir)
        {
            item.interactable = false;
        }
        if (pHabilidad != null && !(habilidadescargadas.Contains(pHabilidad)))
        {

            RestablecerEnfriamientoYTeclas();
            if (!habilidadescargadas.Contains(pHabilidad))
            {
                habilidadescargadas.Add(pHabilidad);
            }
            if (todaslashabilidades.Contains(pHabilidad))
            {
                todaslashabilidades.Remove(pHabilidad);
            }
            
            CargarRandomNumber();
            GameControllerSI._gameControllerLvL1.PausarJuego = false;
            if (habilidadesEnMano.Count < 3)
            {
                DesbloquearHabilidadPorNombre(pHabilidad.nombre.ToString());
                habilidadesEnMano.Add(pHabilidad);

            }
            DescripcionHabilidades.SetActive(false);

            _DesbloquearHabilidad.SetActive(false);
            HabilidadesManager._Persistencia.GuardarHabilidades();
            GameControllerSI._gameControllerLvL1.PausarJuego = false;
            return;
        }
        if (pHabilidad != null && habilidadescargadas.Contains(pHabilidad))
        {
            if (habilidadesEnMano.Count < 3 && !(habilidadesEnMano.Contains(pHabilidad)))
            {
                RestablecerEnfriamientoYTeclas();
                DesbloquearHabilidadPorNombre(pHabilidad.nombre.ToString());
                habilidadesEnMano.Add(pHabilidad);
                DescripcionHabilidades.SetActive(false);
                _DesbloquearHabilidad.SetActive(false);
            }

        }


    }
    public void DesbloquearHabilidadPorNombre(string nombreHabilidad)
    {
        foreach (Habilidad habilidad in habilidadescargadas)
        {
            string NombreHabilidad = habilidad.nombre.ToString();
            if (NombreHabilidad == nombreHabilidad)
            {
                TipoDeHabilidad habilidadConvertida = (TipoDeHabilidad)Enum.Parse(typeof(TipoDeHabilidad), nombreHabilidad);
                _jugadorhabilidades.DesbloquearHabilidad(habilidadConvertida);
                Debug.Log("cargo" + nombreHabilidad);
                break;
            }
        }
    }
    public void RestablecerEnfriamientoYTeclas()
    {
        foreach (var item in _ArbolHabilidadesSI.habilidadescargadas)
        {
            item.tecla = KeyCode.None;
            item.actualenfriamiento = 0;
        }
        foreach (var item in _ArbolHabilidadesSI.todaslashabilidades)
        {
            item.tecla = KeyCode.None;
            item.actualenfriamiento = 0;
        }
    }
    public void CargarTeclasHabilidadesMano()
    {
        if (habilidadesEnMano.Count >= 1)
        {
            if (habilidadesEnMano[0].tecla == KeyCode.None)
            {

                habilidadesEnMano[0].tecla = KeyCode.Q;
            }
        }
        if (habilidadesEnMano.Count >= 2)
        {
            if (habilidadesEnMano[1].tecla == KeyCode.None)
            {
                habilidadesEnMano[1].tecla = KeyCode.E;
            }
        }
        if (habilidadesEnMano.Count == 3)
        {
            if (habilidadesEnMano[2].tecla == KeyCode.None)
            {
                habilidadesEnMano[2].tecla = KeyCode.R;
            }
        }

    }

}
