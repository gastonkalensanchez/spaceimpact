using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnfriamientoManager : MonoBehaviour
{
    public static EnfriamientoManager intancia;
    public List<Habilidad> habilidadesenenfriamiento = new List<Habilidad>();
    private void Awake()
    {
        if (intancia == null)
        {
            intancia = this;
        }
 
     
    }

    void Update()
    {
        for (int i = 0; i < habilidadesenenfriamiento.Count; i++)
        {
            habilidadesenenfriamiento[i].actualenfriamiento -= Time.deltaTime;
            if (habilidadesenenfriamiento[i].actualenfriamiento <=0)
            {
                habilidadesenenfriamiento[i].actualenfriamiento = 0;
                habilidadesenenfriamiento.Remove(habilidadesenenfriamiento[i]);
            }
        }
    }
    public void EmpezarEnfriamiento(Habilidad habilidad)
    {
        if (!habilidadesenenfriamiento.Contains(habilidad))
        {
            habilidad.actualenfriamiento = habilidad.maximoenfriamiento;
            habilidadesenenfriamiento.Add(habilidad);
        }
    }
}
