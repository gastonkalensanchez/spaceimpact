using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
public class Jugador : MonoBehaviour
{

    public GameObject Particulas;
    #region Disparos
    internal Camera cam;
    public Transform puntodedisparo;
    public GameObject proyectil;
    public GameObject Cohete;
    internal float Restroceso;
    internal float Restrocesoentredisparos;
    bool _disparo;
    internal float fuerzadisparo;

    #endregion
    #region Habilidades
    public bool TireCohetes;
    private JugadorHabilidades jugadorhabilidades;
    public bool clon;
    internal bool evade;
    public bool Escudado;
    internal bool inmortal;
    internal int DestruirPro;
    public int cantidaddedisparos = 0;
    #endregion
    #region Movimiento
    internal float x, mx, y, my;
    internal float velocidad;
    public int Direccion;
    #endregion
    #region Animacion

    public Animator animator;
    private string estado;

    #endregion
    public TMP_Text textovida;
    public int vida;
    public float auxrecibidaño;
    public Material material;
    public Material _MaterialJ;
    public float AuxMuerte;
    public bool Murio;
    public AudioSource disparo;
    public AudioSource sonmuerte;
    public bool Nivel2;
    public GameObject Pj;
    public GameObject Continuar;

    private float tiempoescudo;
    float auxinmortal;
    private bool pegoescudo;
    private void Awake()
    {
        if (!clon)
        {
            cam = Camera.main;
            jugadorhabilidades = new JugadorHabilidades();
        }
    }
    void Start()
    {
        material.color = Color.white;
        Murio = false;
        AuxMuerte = 0;
        vida = 3;
        AsignarValorVariables();

    }

    void FixedUpdate()
    {

        if (!clon && !GameControllerSI._gameControllerLvL1.PausarJuego && !TireCohetes)
        {
            Movimiento();
        }

    }
    void Update()
    {
        if (pegoescudo)
        {
            tiempoescudo += Time.deltaTime;
            inmortal = true;

        }
        if (tiempoescudo >= 0.5f)
        {
            inmortal = false;
            pegoescudo = false;
            tiempoescudo = 0;
        }
        if (inmortal)
        {
            auxinmortal += Time.deltaTime;
        }
        if (inmortal && auxinmortal >= 0.5f)
        {
            inmortal = false;
        }


        if (Murio)
        {
            AuxMuerte += Time.deltaTime;
            if (AuxMuerte >= 3)
            {

                Continuar.SetActive(true);
                GameControllerSI._gameControllerLvL1.PausarJuego = true;

            }
        }

        if (!GameControllerSI._gameControllerLvL1.PausarJuego)
        {
            if (transform.position.y > -2)
            {
                if (Input.GetAxisRaw("Horizontal") > 0) // Movimiento a la derecha
                {
                    cambiarestadoanimacion("JugadorCorrerDerecha");
                }
                else if (Input.GetAxisRaw("Horizontal") < 0) // Movimiento a la izquierda
                {
                    cambiarestadoanimacion("JugadorCorrerIzquierda");
                }
                else // No hay movimiento horizontal
                {
                    cambiarestadoanimacion("JugadorCorrerAire");
                }
            }
            else
            {
                cambiarestadoanimacion("JugadorCorrer");
            }
            
            PlayerGameController();

        }


    }
    public void volveraintentar()
    {

    }
    public void cambiarestadoanimacion(string nuevoestado)
    {
        if (estado == nuevoestado)
        {
            return;
        }
        animator.Play(nuevoestado);
        estado = nuevoestado;
    }
    public void PlayerGameController()
    {

        if (!Escudado)
        {
            if (!TireCohetes)
            {
                Disparo();
            }
            
        }



        if (!clon)
        {
            ActualizarTextos();
        }





    }

    void Movimiento()
    {
        cam.transform.parent = null;
        float movimientohorizontal = Input.GetAxis("Horizontal");
        float movimientoab = Input.GetAxis("Vertical");
        Vector3 RotacionCamara = cam.transform.rotation.eulerAngles;
        float xClamped = Mathf.Clamp(RotacionCamara.x, 0, 360);
        float umbral = 0.1f;

        if (!Nivel2)
        {
            if (cam.transform.position.y > -0.5f && Input.GetAxisRaw("Vertical") < 0)
            {
                float nuevaPosY = Mathf.Clamp(cam.transform.position.y - 7 * Time.deltaTime, -0.6f, 2.5f);
                cam.transform.position = new Vector3(cam.transform.position.x, nuevaPosY, cam.transform.position.z);
            }

            if (cam.transform.position.y < 2.5f && Input.GetAxisRaw("Vertical") > 0)
            {
                float nuevaPosY = Mathf.Clamp(cam.transform.position.y + 7 * Time.deltaTime, 0, 2.5f);
                cam.transform.position = new Vector3(cam.transform.position.x, nuevaPosY, cam.transform.position.z);
            }

            if (xClamped < 8 && Input.GetAxisRaw("Vertical") > 0)
            {
                cam.transform.Rotate(0.3f, 0, 0);
            }
            else if (xClamped > 0 && Input.GetAxisRaw("Vertical") < 0)
            {
                cam.transform.Rotate(-0.3f, 0, 0);
            }

            if (xClamped > 0 && xClamped < umbral)
            {
                cam.transform.rotation = Quaternion.Euler(0, RotacionCamara.y, RotacionCamara.z);
            }
            else if (xClamped < 8 && xClamped > 8 - umbral)
            {
                cam.transform.rotation = Quaternion.Euler(8, RotacionCamara.y, RotacionCamara.z);
            }
        }




        if (transform.position.x < x || transform.position.x < mx)
        {
         
            if (Input.GetKey(KeyCode.A))
            {
                transform.Translate(movimientohorizontal * velocidad * Time.deltaTime, 0, 0);
            }
            if (Input.GetKey(KeyCode.D))
            {
                transform.Translate(movimientohorizontal * velocidad * Time.deltaTime, 0, 0);
            }
            if (Input.GetKey(KeyCode.W))
            {
                transform.Translate(0, movimientoab * velocidad * Time.deltaTime, 0);
            }
            if (Input.GetKey(KeyCode.S))
            {
                transform.Translate(0, movimientoab * velocidad * Time.deltaTime, 0);
            }
            

            Direccion = 0;
        }
        if (transform.position.x > x)
        {
            transform.position = new Vector3(x - 0.1f, transform.position.y, transform.position.z);
        }
        if (transform.position.x < mx)
        {
            transform.position = new Vector3(mx + 0.1f, transform.position.y, transform.position.z);
        }

        if (transform.position.y > y)
        {
            transform.position = new Vector3(transform.position.x, y - 0.1f, transform.position.z);
        }
        if (transform.position.y < my)
        {
            transform.position = new Vector3(transform.position.x, my - 0.1f, transform.position.z);
        }

        if (transform.position.x >= 4)
        {
            Direccion = 1;
        }
        if (transform.position.x <= -4)
        {
            Direccion = -1;
        }
        if (transform.position.x >= -4 && transform.position.x <= 4)
        {
            Direccion = 0;
        }


    }

    void AsignarValorVariables()
    {
        Restroceso = 0.3f;
        Restrocesoentredisparos = 0.05f;
        velocidad = 15f;
        x = 5.4f;
        mx = -5.4f;
        y = 2.3f;
        my = -2f;
        fuerzadisparo = 100;
        evade = false;
        Escudado = false;
        DestruirPro = 5;



    }
    void ActualizarTextos()
    {
        if (inmortal)
        {
            textovida.text = "INMORTAL";
        }
        else
        {
            textovida.text = " ";
        }
    }

    public void RecibiDaño()
    {
        if (vida > 0)
        {

            //transform.position = new Vector3(transform.position.x, -2.5f, transform.position.z);
            GameControllerSI._gameControllerLvL1.PausarJuego = true;
            if (!Nivel2)
            {
                cambiarestadoanimacion("JugadorMuerte");
            }
            if (Nivel2)
            {
                Pj.SetActive(false);
            }
            sonmuerte.Play();
            StartCoroutine(RecibidañoE());
        }
        else
        {

            //transform.position = new Vector3(transform.position.x, -2.5f, transform.position.z);
            GameControllerSI._gameControllerLvL1.PausarJuego = true;
            if (!Nivel2)
            {
                cambiarestadoanimacion("JugadorMuerte");
            }
            if (Nivel2)
            {
                Pj.SetActive(false);
            }
            sonmuerte.Play();
            GameControllerSI._gameControllerLvL1.habilidadesManager.GuardarPuntajeMaximo();
            Murio = true;






        }
    }
    IEnumerator RecibidañoE()
    {
        GetComponent<CapsuleCollider>().enabled = false;
        vida -= 1;
        yield return new WaitForSecondsRealtime(5f);
        Pj.SetActive(true);
        inmortal = true;
        cambiarestadoanimacion("JugadorCorrer");
        GameControllerSI._gameControllerLvL1.PausarJuego = false;
        material.color = Color.black;
        yield return new WaitForSecondsRealtime(0.1f);
        material.color = Color.white;
        yield return new WaitForSecondsRealtime(0.1f);
        material.color = Color.black;
        yield return new WaitForSecondsRealtime(0.1f);
        material.color = Color.white;
        yield return new WaitForSecondsRealtime(0.1f);
        material.color = Color.black;
        yield return new WaitForSecondsRealtime(0.1f);
        material.color = Color.white;
        yield return new WaitForSecondsRealtime(0.1f);
        material.color = Color.white;
        yield return new WaitForSecondsRealtime(0.1f);
        material.color = Color.black;
        yield return new WaitForSecondsRealtime(0.1f);
        material.color = Color.white;
        yield return new WaitForSecondsRealtime(0.1f);
        material.color = Color.black;
        yield return new WaitForSecondsRealtime(0.1f);
        material.color = Color.white;
        inmortal = false;
        GetComponent<CapsuleCollider>().enabled = true;

    }

    private void OnCollisionEnter(Collision collision)
    {


        if (collision.gameObject.CompareTag("Estructura") && !evade && !Escudado && !inmortal)
        {
            inmortal = true;
            Instantiate(Particulas, transform.position, Quaternion.identity);
            Destroy(collision.gameObject);
            RecibiDaño();
            if (!Escudado)
            {
                //Destroy(this.gameObject);
            }

        }
        else if (collision.gameObject.CompareTag("Estructura") && Escudado && !inmortal)
        {
            Instantiate(Particulas, transform.position, Quaternion.identity);
            pegoescudo = true;
            Destroy(collision.gameObject);
            RecibirDañoEscudo();

        }
        else if (collision.gameObject.CompareTag("Estructura") && evade && !inmortal)
        {
            Instantiate(Particulas, transform.position, Quaternion.identity);
            Destroy(collision.gameObject);
        }

        if (collision.gameObject.CompareTag("EnemigoBala") && !evade && !Escudado && !inmortal)
        {
            inmortal = true;
            Instantiate(Particulas, transform.position, Quaternion.identity);
            Destroy(collision.gameObject);

            RecibiDaño();
            if (!Escudado)
            {
                //Destroy(this.gameObject);
            }
        }
        else if (collision.gameObject.CompareTag("EnemigoBala") && (evade || Escudado) && !inmortal)
        {
            Instantiate(Particulas, transform.position, Quaternion.identity);
            Destroy(collision.gameObject);
            pegoescudo = true;
            RecibirDañoEscudo();
        }


    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("EnemigoBala") && (!evade && !Escudado) && !inmortal)
        {
            inmortal = true;
            Instantiate(Particulas, transform.position, Quaternion.identity);
            Destroy(other.gameObject);


            if (!Escudado)
            {
                //Destroy(this.gameObject);
            }
        }
        else if (other.gameObject.CompareTag("EnemigoBala") && (evade || Escudado) && !inmortal)
        {
            Instantiate(Particulas, transform.position, Quaternion.identity);
            Destroy(other.gameObject);
            pegoescudo = true;
            RecibirDañoEscudo();
        }
    }
    public JugadorHabilidades GetJugadorHabilidades()
    {
        return jugadorhabilidades;
    }
    public bool TieneLaHabilidad(TipoDeHabilidad tipo)
    {
        return jugadorhabilidades.EstaDebloqueadaLaHabilidad(tipo);
    }

    void Disparo()
    {
        if (Input.GetMouseButtonDown(0) && !_disparo)
        {
            StartCoroutine(Disparar());
            _disparo = true;
        }
    }
    IEnumerator Disparar()
    {
        yield return new WaitForSecondsRealtime(Restroceso); //0.2
        for (int i = 0; i < 3; i++)
        {
            GameObject pro;
            pro = Instantiate(proyectil, puntodedisparo.position, puntodedisparo.rotation);
            pro.GetComponent<Rigidbody>().AddForce(puntodedisparo.forward * fuerzadisparo, ForceMode.Impulse);
            Destroy(pro, DestruirPro);

            yield return new WaitForSecondsRealtime(Restrocesoentredisparos); //0.05
        }
        disparo.Play();
        if (cantidaddedisparos < 10)
            cantidaddedisparos++;
        _disparo = false;
    }
    public void RecibirDañoEscudo()
    {
        GetComponent<ThabilidadesUpdate>().mEscudo.SetActive(false);
        Escudado = false;
        foreach (var habilidad in ArbolHabilidadesSI._ArbolHabilidadesSI.habilidadesEnMano)
        {
            if (habilidad.nombre.ToString() == "Escudo")
            {
                habilidad.PonerEnEnfriamiento();
                break;
            }
        }
    }
}
