using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JugadorEstetica : MonoBehaviour
{
    public GameObject RadioAliados;
    public GameObject[] BolasRotatorias;
    public GameObject[] PuntosCohetes;
    public GameObject PinturaDash;
    public GameObject PinturaEsucudo;
    public GameObject[] Cruz;
    public GameObject ArmaActual,ArmaCohete;
    public GameObject BotaEvacion1,BotaEvacion2;

    
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (this.GetComponent<Jugador>().TieneLaHabilidad(TipoDeHabilidad.Aliados))
        {
            RadioAliados.SetActive(true);
        }
        else
        {
            RadioAliados.SetActive(false);
        }
        if (this.GetComponent<Jugador>().TieneLaHabilidad(TipoDeHabilidad.Bolas))
        {
            foreach (var item in BolasRotatorias)
            {
                item.SetActive(true);
                item.transform.RotateAround(this.transform.position,Vector3.up,50 * Time.deltaTime);
            }
        }
        else
        {
            foreach (var item in BolasRotatorias)
            {
                item.SetActive(false);
            }
        }
        if (this.GetComponent<Jugador>().TieneLaHabilidad(TipoDeHabilidad.Cohetes))
        {
            foreach (var item in PuntosCohetes)
            {
                item.SetActive(true);
            }
        }
        else
        {
            foreach (var item in PuntosCohetes)
            {
                item.SetActive(false);
            }
        }
        if (this.GetComponent<Jugador>().TieneLaHabilidad(TipoDeHabilidad.Dash))
        {
            PinturaDash.SetActive(true);   
        }
        else
        {
            PinturaDash.SetActive(false); 
        }
        if (this.GetComponent<Jugador>().TieneLaHabilidad(TipoDeHabilidad.Escudo))
        {
            PinturaEsucudo.SetActive(true);
        }
        else
        {
            PinturaEsucudo.SetActive(false);
        }
        if (this.GetComponent<Jugador>().TieneLaHabilidad(TipoDeHabilidad.PasivaCohete))
        {
            ArmaActual.SetActive(false);
            ArmaCohete.SetActive(true);
        }
        else
        {
            ArmaActual.SetActive(true);
            ArmaCohete.SetActive(false);
        }
        if (this.GetComponent<Jugador>().TieneLaHabilidad(TipoDeHabilidad.Evacion))
        {
            BotaEvacion1.SetActive(true);
            BotaEvacion2.SetActive(true);
        }
        else
        {
            BotaEvacion1.SetActive(false);
            BotaEvacion2.SetActive(false);
        }
        if (this.GetComponent<Jugador>().TieneLaHabilidad(TipoDeHabilidad.PotenciarBala))
        {
            ArmaActual.GetComponent<Renderer>().material.color = Color.red;
            ArmaCohete.GetComponent<Renderer>().material.color = Color.red;
        }
        else   
        {
            ArmaActual.GetComponent<Renderer>().material.color = Color.white;
            ArmaCohete.GetComponent<Renderer>().material.color = Color.white;
        }
  
           
    
        
    }
}
