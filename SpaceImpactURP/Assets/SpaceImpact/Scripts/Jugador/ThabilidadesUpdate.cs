using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThabilidadesUpdate : MonoBehaviour
{

    #region HabilidadEscudo
    public GameObject mEscudo;
    #endregion
    #region HabilidadCohete
    private bool HabilidadCohete;
    public Transform[] PuntosDeCohete;
    #endregion
    #region HabilidadEvacion
    bool usandoevacion = false;
    float segundosevadiendo;
    Renderer render;
    #endregion    
    #region HabilidadAliados
    public CapsuleCollider Aliado1, Aliado2;
    bool usandoaliado = false;
    public GameObject clon;
    #endregion
    #region HabilidadPotenciarBala
    bool usandopotenciarbala;

    #endregion
    /*
    #region Habilidad Ataque
    bool usandoataque;
    #endregion
    */
    #region Habilidad Bolas
    bool habilidadbolas;
    bool bolaslistas;
    bool seguirenemigos;
    List<GameObject> bolas = new List<GameObject>();
    GameObject enemigo;
    #endregion
    #region Habilidad Inmortal
    bool usandoInmortal;
    #endregion
    #region Habilidad Dash
    #endregion
    #region pasiva CohetePorDisparo
    #endregion
    #region Pasiva Ataque despues de una habilidad
    #endregion

    public Jugador scriptjugador;
    void Start()
    {
        scriptjugador = gameObject.GetComponent<Jugador>();

    }

    void Update()
    {

        // Comprobar las teclas presionadas
        if (Input.GetKeyDown(KeyCode.Q) && !GameControllerSI._gameControllerLvL1.PausarJuego && !usandopotenciarbala)
        {
            EjecutarHabilidades(KeyCode.Q);
        }
        if (Input.GetKeyDown(KeyCode.E) && !GameControllerSI._gameControllerLvL1.PausarJuego && !usandopotenciarbala)
        {
            EjecutarHabilidades(KeyCode.E);
        }
        if (Input.GetKeyDown(KeyCode.R) && !GameControllerSI._gameControllerLvL1.PausarJuego && !usandopotenciarbala)
        {
            EjecutarHabilidades(KeyCode.R);
        }

        if (scriptjugador.TieneLaHabilidad(TipoDeHabilidad.Bolas) && bolaslistas)
        {
            Bolas(bolas, GetHabilidadBolas());
        }
        if (scriptjugador.TieneLaHabilidad(TipoDeHabilidad.PasivaCohete))
        {
            PasivaCohetePorDisparo();
        }


    }



    void EjecutarHabilidades(KeyCode tecla)
    {

        foreach (var habilidad in ArbolHabilidadesSI._ArbolHabilidadesSI.habilidadesEnMano)
        {
            if (habilidad.tecla == tecla)
            {
                EjecutarHabilidad(habilidad);
            }

        }

    }

    void EjecutarHabilidad(Habilidad habilidad)
    {
        if (!habilidad.HabilidadLista())
        {
            // La habilidad no está lista, no se ejecuta
            return;
        }

        switch (habilidad.nombre.ToString())
        {
            case "Escudo":
                if (scriptjugador.TieneLaHabilidad(TipoDeHabilidad.Escudo))
                {
                    _HabilidadEscudo(habilidad);

                }
                break;
            case "Cohetes":
                if (scriptjugador.TieneLaHabilidad(TipoDeHabilidad.Cohetes) && !scriptjugador.TieneLaHabilidad(TipoDeHabilidad.PasivaCohete))
                {
                    _HabilidadCohete(habilidad);

                }
                break;
            case "Evacion":
                if (scriptjugador.TieneLaHabilidad(TipoDeHabilidad.Evacion))
                {
                    _HabilidadEvacion(habilidad);

                }
                break;
            case "Aliados":
                if (scriptjugador.TieneLaHabilidad(TipoDeHabilidad.Aliados))
                {
                    _HabilidadAliados(habilidad);

                }
                break;
            case "PotenciarBala":
                if (scriptjugador.TieneLaHabilidad(TipoDeHabilidad.PotenciarBala))
                {
                    _HabilidadPotenciarBala(habilidad);

                }
                break;
            /*
            case "Ataque":
                if (scriptjugador.TieneLaHabilidad(TipoDeHabilidad.Ataque))
                {
                    _TiroAtaque(habilidad);


                }
                break;

            */
            case "Bolas":
                if (scriptjugador.TieneLaHabilidad(TipoDeHabilidad.Bolas))
                {
                    _HabilidadBolas();

                }
                break;
            /*
            case "Inmortal":
                if (scriptjugador.TieneLaHabilidad(TipoDeHabilidad.Inmortal))
                {
                    _HabilidadInmortal(habilidad);

                }
                break;
            */
            case "Dash":
                if (scriptjugador.TieneLaHabilidad(TipoDeHabilidad.Dash))
                {
                    _HabilidadDash(habilidad);

                }
                break;
            default:
                break;
        }
    }

    void _HabilidadEscudo(Habilidad habilidad)
    {
        if (scriptjugador.Escudado)
        {

            scriptjugador.RecibirDañoEscudo();
            return;
        }
        mEscudo.SetActive(true);
        scriptjugador.Escudado = true;
    }
    Habilidad GetHabilidadEscudo()
    {
        foreach (var item in ArbolHabilidadesSI._ArbolHabilidadesSI.habilidadesEnMano)
        {
            if (item.nombre.ToString() == "Escudo")
            {
                return item;
            }
        }
        return null;
    }

    void _HabilidadCohete(Habilidad habilidad)
    {

        if (HabilidadCohete)
        {
            return;
        }
        GetComponent<Jugador>().TireCohetes = true;
        StartCoroutine(TirarCohete(habilidad));
    }

    IEnumerator TirarCohete(Habilidad habilidad)
    {

        HabilidadCohete = true;
        habilidad.PonerEnEnfriamiento();
        for (int i = 0; i < 4; i++)
        {
            yield return new WaitForSeconds(0.2f);
            for (int j = 0; j < PuntosDeCohete.Length; j++)
            {
                GameObject pro;
                pro = Instantiate(scriptjugador.Cohete, PuntosDeCohete[j].position, PuntosDeCohete[j].rotation);
                pro.GetComponent<Rigidbody>().AddForce(PuntosDeCohete[j].forward * 100, ForceMode.Impulse);
                pro.GetComponent<Rigidbody>().AddForce(new Vector3(10, 10, 10));
                Destroy(pro, 2.5f);
            }


        }
        habilidad.PonerEnEnfriamiento();
        HabilidadCohete = false;
        GetComponent<Jugador>().TireCohetes = false;


    }

    void _HabilidadEvacion(Habilidad habilidad)
    {
        if (usandoevacion)
        {
            // La evasión ya está en uso, no se ejecuta
            return;
        }
        scriptjugador.evade = true;
        segundosevadiendo = 5;
        StartCoroutine(Evacion(habilidad));
    }

    IEnumerator Evacion(Habilidad habilidad)
    {
        GetComponent<Jugador>().velocidad = 8;
        usandoevacion = true;
        Material render = scriptjugador._MaterialJ;
        render.color = Color.green;
        yield return new WaitForSecondsRealtime(segundosevadiendo);
        render.color = Color.white;
        scriptjugador.evade = false;
        habilidad.PonerEnEnfriamiento();
        usandoevacion = false;
        GetComponent<Jugador>().velocidad = 15;
    }

    void _HabilidadAliados(Habilidad habilidad)
    {
        if (usandoaliado)
        {
            return;
        }
        StartCoroutine(Aliado(habilidad));

    }
    IEnumerator Aliado(Habilidad habilidad)
    {
        usandoaliado = true;
        GameObject Al = Instantiate(clon, new Vector3(transform.position.x + 2, transform.position.y, transform.position.z), Quaternion.identity);
        GameObject Al2 = Instantiate(clon, new Vector3(transform.position.x - 2, transform.position.y, transform.position.z), Quaternion.identity);
        Al.GetComponent<Jugador>().clon = true;
        Al2.GetComponent<Jugador>().clon = true;
        Al.transform.parent = this.transform;
        Al2.transform.parent = this.transform;
        Aliado1.enabled = true;
        Aliado2.enabled = true;
        yield return new WaitForSecondsRealtime(2f);
        Aliado1.enabled = false;
        Aliado2.enabled = false;
        Destroy(Al);
        Destroy(Al2);
        usandoaliado = false;
        habilidad.PonerEnEnfriamiento();
    }
    void _HabilidadPotenciarBala(Habilidad habilidad)
    {
        if (usandopotenciarbala)
        {
            return;
        }
        StartCoroutine(PotenciarBalas(habilidad));
    }
    IEnumerator PotenciarBalas(Habilidad habilidad)
    {
        foreach (var item in GetComponent<JugadorEstetica>().Cruz)
        {
            item.SetActive(true);
        }
        GetComponent<Jugador>().velocidad = 8;
        usandopotenciarbala = true;
        scriptjugador.Restroceso = 0.05f;
        scriptjugador.Restrocesoentredisparos = 0;
        yield return new WaitForSecondsRealtime(5f);
        scriptjugador.Restroceso = 0.2f;
        scriptjugador.Restrocesoentredisparos = 0.05f;
        usandopotenciarbala = false;
        GetComponent<Jugador>().velocidad = 15;
        foreach (var item in GetComponent<JugadorEstetica>().Cruz)
        {
            item.SetActive(false);
        }
        habilidad.PonerEnEnfriamiento();
    }
    /*
    void _TiroAtaque(Habilidad habilidad)
    {
        if (usandoataque)
        {
            return;
        }
        StartCoroutine(TirarAtaque(habilidad));
    }
    IEnumerator TirarAtaque(Habilidad habilidad)
    {
        usandoataque = true;
        habilidad.PonerEnEnfriamiento();
        for (int i = 0; i < 3; i++)
        {
            yield return new WaitForSeconds(0.1f);
            GameObject pro = Instantiate(scriptjugador.proyectil, scriptjugador.puntodedisparo.position, Quaternion.identity);
            pro.transform.localScale = new Vector3(70, 70, 70);
            pro.tag = "Cohete";
            pro.GetComponent<Renderer>().material.color = Color.red;
            pro.GetComponent<Rigidbody>().AddForce(scriptjugador.puntodedisparo.forward * scriptjugador.fuerzadisparo, ForceMode.Impulse);
            Destroy(pro, scriptjugador.DestruirPro);
        }
        habilidad.PonerEnEnfriamiento();
        usandoataque = false;
    }
    */
    void _HabilidadBolas()
    {
        if (habilidadbolas)
        {
            return;
        }
        Bolas();
    }
    void Bolas()
    {
        habilidadbolas = true;
        GameObject pro = Instantiate(scriptjugador.proyectil, new Vector3(transform.position.x + 4, transform.position.y, transform.position.z), Quaternion.identity);
        GameObject pro2 = Instantiate(scriptjugador.proyectil, new Vector3(transform.position.x - 4, transform.position.y, transform.position.z), Quaternion.identity);
        GameObject pro3 = Instantiate(scriptjugador.proyectil, new Vector3(transform.position.x, transform.position.y, transform.position.z + 4), Quaternion.identity);
        pro.transform.localScale = new Vector3(90, 90, 90);
        pro.GetComponent<Renderer>().material.color = Color.red;
        pro.GetComponent<SphereCollider>().isTrigger = true;
        pro.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        pro2.transform.localScale = new Vector3(90, 90, 90);
        pro2.GetComponent<Renderer>().material.color = Color.red;
        pro.GetComponent<SphereCollider>().isTrigger = true;
        pro2.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        pro3.transform.localScale = new Vector3(90, 90, 90);
        pro.GetComponent<SphereCollider>().isTrigger = true;
        pro3.GetComponent<Renderer>().material.color = Color.red;
        pro3.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        pro.transform.parent = this.transform;
        pro2.transform.parent = this.transform;
        pro3.transform.parent = this.transform;
        bolas.Add(pro);
        bolas.Add(pro2);
        bolas.Add(pro3);
        bolaslistas = true;

    }
    void Bolas(List<GameObject> lista, Habilidad habilidad)
    {
        if (lista.Count == 0)
        {
            bolaslistas = false;
            habilidadbolas = false;
            habilidad.PonerEnEnfriamiento();
            return;
        }
        if (GameControllerSI._gameControllerLvL1.EnemigosEnScena.Count > 0 && enemigo == null)
        {
            enemigo = GameControllerSI._gameControllerLvL1.EnemigosEnScena[Random.Range(0, GameControllerSI._gameControllerLvL1.EnemigosEnScena.Count)];
        }
        if (GameControllerSI._gameControllerLvL1.EnemigosEnScena.Count >= 0 && GameControllerSI._gameControllerLvL1._InstanciaJefe != null)
        {
            enemigo = GameControllerSI._gameControllerLvL1._InstanciaJefe;
        }
        foreach (var item in lista)
        {
            if (item != null)
            {
                item.transform.Rotate(0, 2, 0);
            }
            else
            {
                lista.Remove(item);
                return;
            }
        }

        if (enemigo != null)
        {
            seguirenemigos = true;
        }
        if (seguirenemigos && enemigo != null)
        {
            foreach (var item in lista)
            {
                if (item != null)
                {
                    item.transform.position = Vector3.MoveTowards(item.transform.position, enemigo.transform.position, 20 * Time.deltaTime);
                }
            }
            habilidad.PonerEnEnfriamiento();

        }


    }
    Habilidad GetHabilidadBolas()
    {
        foreach (var item in ArbolHabilidadesSI._ArbolHabilidadesSI.habilidadesEnMano)
        {
            if (item.nombre.ToString() == "Bolas")
            {
                return item;
            }
        }
        return null;
    }
    /*
    void _HabilidadInmortal(Habilidad habilidad)
    {
        if (usandoInmortal)
        {
            return;
        }
        StartCoroutine(Inmortal(habilidad));
    }
    IEnumerator Inmortal(Habilidad habilidad)
    {
        usandoInmortal = true;
        scriptjugador.inmortal = true;
        yield return new WaitForSecondsRealtime(3f);
        scriptjugador.inmortal = false;
        usandoInmortal = false;
        habilidad.PonerEnEnfriamiento();

    }
    */
    void _HabilidadDash(Habilidad habilidad)
    {
        Dash(habilidad);
    }
    void Dash(Habilidad habilidad)
    {

        if (Input.GetAxisRaw("Vertical") > 0)
        {
            transform.Translate(0, 2 * 5, 0);
            habilidad.PonerEnEnfriamiento();
        }
        if (Input.GetAxisRaw("Vertical") < 0)
        {
            transform.Translate(0, -2 * 5, 0);
            habilidad.PonerEnEnfriamiento();
        }
        if (Input.GetAxisRaw("Horizontal") < 0)
        {
            transform.Translate(-2 * 5, 0, 0);
            habilidad.PonerEnEnfriamiento();
        }
        if (Input.GetAxisRaw("Horizontal") > 0)
        {
            transform.Translate(2 * 5, 0, 0);
            habilidad.PonerEnEnfriamiento();
        }

    }
    void PasivaCohetePorDisparo()
    {
        if (scriptjugador.cantidaddedisparos == 10)
        {
            GameObject cohe;
            cohe = Instantiate(scriptjugador.Cohete, scriptjugador.puntodedisparo.position, scriptjugador.puntodedisparo.rotation);
            cohe.GetComponent<Rigidbody>().AddForce(scriptjugador.puntodedisparo.forward * scriptjugador.fuerzadisparo, ForceMode.Impulse);
            scriptjugador.cantidaddedisparos -= 10;
            Destroy(cohe, 5);
        }
    }







}
