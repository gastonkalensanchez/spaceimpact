using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TodasLasHabilidades : MonoBehaviour
{
    public bool Aliado,Ataque,Bolas,Cohete,Dash,Escudo,Evacion,Inmortal,PotenciarBala,Rafaga;
    public bool PCohete,PLuegoDeUnaHabilidad,PRevivir,PVelocidadAtaque;
   
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (this.GetComponent<Jugador>().TieneLaHabilidad(TipoDeHabilidad.Aliados))
        {
            Aliado = true;
        }
        if (this.GetComponent<Jugador>().TieneLaHabilidad(TipoDeHabilidad.Ataque))
        {
            Ataque = true;
        }
        if (this.GetComponent<Jugador>().TieneLaHabilidad(TipoDeHabilidad.Bolas))
        {
            Bolas = true;
        }
        if (this.GetComponent<Jugador>().TieneLaHabilidad(TipoDeHabilidad.Cohetes))
        {
            Cohete = true;
        }
        if (this.GetComponent<Jugador>().TieneLaHabilidad(TipoDeHabilidad.Dash))
        {
            Dash = true;
        }
        if (this.GetComponent<Jugador>().TieneLaHabilidad(TipoDeHabilidad.Escudo))
        {
            Escudo = true;
        }
        if (this.GetComponent<Jugador>().TieneLaHabilidad(TipoDeHabilidad.Evacion))
        {
            Evacion = true;
        }
        if (this.GetComponent<Jugador>().TieneLaHabilidad(TipoDeHabilidad.Inmortal))
        {
            Inmortal = true;
        }
        if (this.GetComponent<Jugador>().TieneLaHabilidad(TipoDeHabilidad.PotenciarBala))
        {
            PotenciarBala = true;
        }
        if (this.GetComponent<Jugador>().TieneLaHabilidad(TipoDeHabilidad.Rafaga))
        {
            Rafaga = true;
        }
        if (this.GetComponent<Jugador>().TieneLaHabilidad(TipoDeHabilidad.PasivaCohete))
        {
            PCohete = true;
        }
        if (this.GetComponent<Jugador>().TieneLaHabilidad(TipoDeHabilidad.LuegoDeUnaHabilidad))
        {
            PLuegoDeUnaHabilidad = true;
        }
        if (this.GetComponent<Jugador>().TieneLaHabilidad(TipoDeHabilidad.Revivir))
        {
            PRevivir = true;
        }
        if (this.GetComponent<Jugador>().TieneLaHabilidad(TipoDeHabilidad.VelocidadDeAtaque))
        {
            PVelocidadAtaque = true;
        }
    }
}
