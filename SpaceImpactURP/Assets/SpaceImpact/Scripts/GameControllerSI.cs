using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class GameControllerSI : MonoBehaviour
{
    public GameObject _Jefe;
    public GameObject _InstanciaJefe;
    private bool _InstancioJefe;
    public static GameControllerSI _gameControllerLvL1;
    public GameObject _Enemigos_PuntosSpawn;
    public Transform[] Enemigos_puntosSpawn;
    public Transform[] Enemigos_SpawnerEnemigos;
    public GameObject[] Enemigos;
    internal GameObject Jugador;
    public float PuntuacionNivel1;
    public float _TiempoDeSpawn;
    public List<GameObject> EnemigosEnScena = new List<GameObject>();
    public float AuxPuntuacion;
    public bool PausarJuego;
    ArbolHabilidadesSI ArbolJugador;
    public float time;
    float tiempoSpawn;
    public bool BossFight;
    public float AuxBossFight;
    public bool MatoAlJefe;
    public bool _Nivel1, _Nivel2, _Nivel3;
    public HabilidadesManager habilidadesManager;
    public AudioSource Nivel1Sound, Nivel2Sound, Nivel3Sound,BossfightSound;
    public TMPro.TMP_Text JefeCons;
    private float auxmouse;
    private bool bloquearmouse;
    public TMPro.TMP_Text Stage;
    void Awake()
    {
        if (_gameControllerLvL1 == null)
        {
            _gameControllerLvL1 = this;
        }


    }
    void Start()
    {
        if (GameObject.FindGameObjectWithTag("Player"))
        {
            Jugador = GameObject.FindGameObjectWithTag("Player");
            ArbolJugador = GameObject.FindGameObjectWithTag("Player").GetComponent<ArbolHabilidadesSI>();
        }

        _TiempoDeSpawn = 10;
        if (_Nivel1)
        {
            Nivel1Sound.Play();
        }
        if (_Nivel2)
        {
            Nivel2Sound.Play();
        }
        if (_Nivel3)
        {
            Nivel3Sound.Play();
        }

    }

    // Update is called once per frame
    void Update()
    {
        foreach (GameObject item in GameObject.FindObjectsOfType<GameObject>())
        {

            if (item.name.Length >= 3 && item.name.Substring(0, 3).Equals("ENE"))
            {
                if (!(EnemigosEnScena.Contains(item)))
                {
                    EnemigosEnScena.Add(item);
                }

            }
        }




        if (BossFight)
        {
            Stage.gameObject.SetActive(false);
            if (_Nivel1)
            {
                Nivel1Sound.Stop();
            }
            if (_Nivel2)
            {
                Nivel2Sound.Stop();
            }
            if (_Nivel3)
            {
                Nivel3Sound.Stop();
            }
            
            if (!_InstancioJefe)
            {
                if (_Nivel3)
                {
                    _InstanciaJefe = Instantiate(_Jefe, new Vector3(0, -5, 0), Quaternion.identity);
                    BossfightSound.Play();
                    _InstancioJefe = true;
                }
                else
                {
                    _InstanciaJefe = Instantiate(_Jefe, new Vector3(0, 0, 120), Quaternion.identity);
                    BossfightSound.Play();
                    _InstancioJefe = true;
                }

            }
            if (EnemigosEnScena.Count > 0)
            {
                foreach (GameObject item in EnemigosEnScena)
                {
                    Destroy(item);
                }
            }       

            if (_InstanciaJefe == null)
            {
                BossFight = false;
            }
        }
        else
        {
            if (!PausarJuego)
            {
                Stage.gameObject.SetActive(true);
            }
            else
            {
                Stage.gameObject.SetActive(false);
            }
            
            JefeCons.text = " ";
            _InstancioJefe = false;
            Nivel1();
        }




    }
    

    void Nivel1()
    {
        if (PausarJuego)
        {
            auxmouse += Time.deltaTime;
            if (auxmouse >= 2)
            {
                bloquearmouse = true;
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
                auxmouse = 0;
            }
            foreach (GameObject item in GameObject.FindObjectsOfType<GameObject>())
            {
                if (item.gameObject.tag == "EnemigoBala")
                {
                    Destroy(item);
                }
            }
        }
        if (AuxBossFight >= 100)
        {
            BossFight = true;
            AuxBossFight = 0;
        }
        if (!PausarJuego)
        {
            bloquearmouse = false;
            AuxBossFight += Time.deltaTime;
            if (_Nivel1)
            {
                ScoreManager.Scoree1 += 50 * Time.deltaTime;
            }
            if (_Nivel2)
            {
                ScoreManager.Scoree2 += 50 * Time.deltaTime;
            }
            if (_Nivel3)
            {
                ScoreManager.Scoree3 += 50 * Time.deltaTime;
            }

            AuxPuntuacion += Time.deltaTime;
            PuntuacionNivel1 +=  50 * Time.deltaTime;
            _Enemigos_PuntosSpawn.transform.position = new Vector3(Jugador.transform.position.x, _Enemigos_PuntosSpawn.transform.position.y, _Enemigos_PuntosSpawn.transform.position.z);
            ControlarSpawnDeEnemigos();

    
            SpawnearEnemigos();
    
        }
        if (AuxPuntuacion >= time && !PausarJuego && ArbolJugador.todaslashabilidades.Count >= 1)
        {
            if (!bloquearmouse)
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }

            PausarJuego = true;
            ArbolJugador.RestablecerEnfriamientoYTeclas();
            ArbolJugador.MostrarHabilidadesParaEleguir();
            ArbolJugador._TodasLasHabilidades.SetActive(false);
            ArbolJugador._DesbloquearHabilidad.SetActive(true);
            AuxPuntuacion = 0;
        }
        if (Input.GetKeyDown(KeyCode.Y) && !PausarJuego)
        {
            if (!bloquearmouse)
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }
            PausarJuego = true;
            VerHabilidadesJugador();
            AuxPuntuacion = 0;
        }





    }
    public void VerHabilidadesJugador()
    {
        ArbolJugador.RestablecerEnfriamientoYTeclas();
        ArbolJugador._TodasLasHabilidades.SetActive(true);
        ArbolJugador._DesbloquearHabilidad.SetActive(false);
        ArbolJugador.habilidadesEnMano.Clear();
        ArbolJugador._jugadorhabilidades.EliminarTodasLasHabilidadesCargadas();
    }

    void ControlarSpawnDeEnemigos()
    {
        if (PuntuacionNivel1 <= 500)
        {
            _TiempoDeSpawn = 10;
        }
        if (PuntuacionNivel1 >= 500 && PuntuacionNivel1 <= 700)
        {
            _TiempoDeSpawn = 8;
        }
        if (PuntuacionNivel1 >= 700 && PuntuacionNivel1 <= 1000)
        {
            _TiempoDeSpawn = 6;
        }
        if (PuntuacionNivel1 >= 1500)
        {
            _TiempoDeSpawn = 5;
        }
    }

    


    void SpawnearEnemigos()
    {
        tiempoSpawn += Time.deltaTime;

        if (tiempoSpawn >= _TiempoDeSpawn)
        {
            for (int i = 0; i < 3; i++)
            {
                StartCoroutine(InstanciarEnemigo(i * 0.4f));
            }

            tiempoSpawn = 0f;
        }
    }

    IEnumerator InstanciarEnemigo(float delay)
    {
        yield return new WaitForSeconds(delay);
        GameObject g = Instantiate(Enemigos[Random.Range(0, Enemigos.Length)], Enemigos_SpawnerEnemigos[Random.Range(0, Enemigos_SpawnerEnemigos.Length)].position, Quaternion.identity);
    }

    private void OnApplicationQuitting()
    {
        GuardarDatos();
    }

    private void GuardarDatos()
    {
        // Aquí guardas tus datos en el formato deseado (por ejemplo, en archivos JSON)
  

        // Ejemplo de guardado de habilidades
        HabilidadesManager._Persistencia.GuardarHabilidades();
        HabilidadesManager._Persistencia.GuardarVariablesJefes();
        HabilidadesManager._Persistencia.GuardarPuntajeMaximo();

        Debug.Log("Datos guardados antes de cerrar la aplicación.");
    }
}
