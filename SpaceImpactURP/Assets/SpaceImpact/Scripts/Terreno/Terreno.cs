using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Terreno : MonoBehaviour
{
    public GameObject[] piso;
    public Vector3 tamañomundo = new Vector3(5, 5, 5);
    Vector3 distancia = new Vector3(80, 2, 80);
    Vector3 pos = Vector3.zero;
    float[] altura;
   

    void Start()
    {     
        IntanciarPisos();
    }
    void IntanciarPisos()
    {
        distancia.x *= tamañomundo.x;
        distancia.z *= tamañomundo.z;

        altura = new float[(int)(distancia.x * distancia.z)];

        for (int i = 0; i < tamañomundo.x * tamañomundo.z; i++) //instancia los objetos
        {
            if (i == 2)
            {
                GameObject nuevopiso = piso[0];
                Instantiate(nuevopiso, Vector3.zero, Quaternion.identity, transform);
            }
            else
            {
                GameObject nuevopiso = piso[Random.Range(0, piso.Length)];
                Instantiate(nuevopiso, Vector3.zero, Quaternion.identity, transform);
            }
           
   
            

        }
        MarcarAltura();
    }
    void MarcarAltura()
    {
      


        
        for (float z = 0; z < distancia.z; z++)
        {
            for (float x = 0; x < distancia.x; x++)
            {

                altura[(int)x + (int)z * (int)distancia.x] = 0; //pone las alturas en 0
            }
        }
        LugarPisos();
    }
    void LugarPisos()
    {
        int i = 0;
        for (pos.z = 0f; pos.z < distancia.z; pos.z++)
        {           
            for (pos.x = 0f; pos.x < distancia.x; pos.x++) //pone a los pisos en cuadriculas para hacer una alfombra
            {
                pos.y = altura[(int)pos.x + (int)pos.z * (int)distancia.x];//toma la altura del arreglo de alturas
                transform.GetChild(i).localPosition = pos;
                i++;
                pos.x += 79;
            }
            pos.z += 79;
        }
    }
    void Update()
    {


        transform.Translate(0, 0, -50 * Time.deltaTime);
    }

 
 

}
