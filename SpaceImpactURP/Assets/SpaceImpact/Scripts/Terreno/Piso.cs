using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Piso : MonoBehaviour
{
    public GameObject[] Objetos;
    [Range(0f, 1f)]
    public float pro = 0.75f;

    void Start()
    {
        if (Random.Range(0f, 1f) <= pro)
        {
            GameObject ob = Instantiate(Objetos[Random.Range(0, Objetos.Length)], new Vector3(transform.position.x, transform.position.y , transform.position.z), Quaternion.Euler(Vector3.up * (Random.Range(0, 4) * 90)));
            ob.transform.parent = this.transform;
        }

    }
  

}
