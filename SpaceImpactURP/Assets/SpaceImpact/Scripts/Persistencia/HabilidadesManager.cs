using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine.UI;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine.SceneManagement;
using Newtonsoft.Json;
using System.Linq;

[System.Serializable]
public class HabilidadesData
{
    public List<Habilidad> habilidades;
}
public class Score
{
    public int HighScores1, HighScores2, HighScores3;
}
public class JefesData
{
    public bool Jefe1Muerto;
    public bool Jefe2Muerto;
    public bool Jefe3Muerto;
}


public class HabilidadesManager : MonoBehaviour
{
    public static HabilidadesManager _Persistencia;
    public ArbolHabilidadesSI arbolHabilidadesSI; // Referencia al script ArbolHabilidadesSI
    public GameControllerSI gameControllerSI;
    public List<Habilidad> HabilidadesCargadas;
    public int HighScore1, HighScore2, HighScore3;
    public bool nivel1, nivel2, nivel3;
    public bool Jefe1Muerto;
    public bool Jefe2Muerto;
    public bool Jefe3Muerto;
    public string NombreDelUsuario;
    private void Awake()
    {
        if (_Persistencia == null)
        {
            _Persistencia = this;
        }

    }
    void Start()
    {
        CargarPuntajeMaximo();
        CargarVariablesJefes();
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.O))
        {
            GuardarPuntajeMaximo();
        }
    }

    public void GuardarVariablesJefes()
    {
        JefesData jefesData = new JefesData();
        jefesData.Jefe1Muerto = Jefe1Muerto;
        jefesData.Jefe2Muerto = Jefe2Muerto;
        jefesData.Jefe3Muerto = Jefe3Muerto;

        string jsonData = JsonUtility.ToJson(jefesData);

        string filePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "JefesData.json");
        File.WriteAllText(filePath, jsonData);

        Debug.Log("Variables de los jefes guardadas correctamente.");
    }

    public void CargarVariablesJefes()
    {
        string filePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "JefesData.json");

        if (File.Exists(filePath))
        {
            string jsonData = File.ReadAllText(filePath);

            JefesData jefesData = JsonUtility.FromJson<JefesData>(jsonData);

            Jefe1Muerto = jefesData.Jefe1Muerto;
            Jefe2Muerto = jefesData.Jefe2Muerto;
            Jefe3Muerto = jefesData.Jefe3Muerto;

            Debug.Log("Variables de los jefes cargadas correctamente.");
        }
        else
        {
            //Debug.Log("No se encontró el archivo de variables de los jefes.");
        }
    }
    public void GuardarPuntajeMaximo()
    {
        if (ScoreManager.Scoree1 > HighScore1 && nivel1)
        {
            HighScore1 = (int)ScoreManager.Scoree1;


            Score data1 = new Score();
            data1.HighScores1 = HighScore1;

            string jsonData = JsonUtility.ToJson(data1);

            string filePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "HighScore1.json");
            File.WriteAllText(filePath, jsonData);

            Debug.Log("Puntaje máximo guardado correctamente.");
        }
        if (ScoreManager.Scoree2 > HighScore2 && nivel2)
        {
            HighScore2 = (int)ScoreManager.Scoree2;


            Score data2 = new Score();
            data2.HighScores2 = HighScore2;

            string jsonData = JsonUtility.ToJson(data2);

            string filePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "HighScore2.json");
            File.WriteAllText(filePath, jsonData);

            Debug.Log("Puntaje máximo guardado correctamente.");
        }
        if (ScoreManager.Scoree3 > HighScore3 && nivel3)
        {
            HighScore3 = (int)ScoreManager.Scoree3;


            Score data3 = new Score();
            data3.HighScores3 = HighScore3;

            string jsonData = JsonUtility.ToJson(data3);

            string filePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "HighScore3.json");
            File.WriteAllText(filePath, jsonData);

            Debug.Log("Puntaje máximo guardado correctamente.");
        }
    }


    public void CargarPuntajeMaximo()
    {
        if (nivel1)
        {
            string filePath1 = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "HighScore1.json");

            if (File.Exists(filePath1))
            {
                string jsonData = File.ReadAllText(filePath1);

                Score data = JsonUtility.FromJson<Score>(jsonData);

                HighScore1 = data.HighScores1;

                Debug.Log("Puntaje máximo cargado correctamente.");
            }
            else
            {
                //Debug.Log("No se encontró el archivo de puntaje máximo.");
            }
        }
        if (nivel2)
        {
            string filePath2 = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "HighScore2.json");

            if (File.Exists(filePath2))
            {
                string jsonData = File.ReadAllText(filePath2);

                Score data = JsonUtility.FromJson<Score>(jsonData);

                HighScore2 = data.HighScores2;

                Debug.Log("Puntaje máximo cargado correctamente.");
            }
            else
            {
                //Debug.Log("No se encontró el archivo de puntaje máximo.");
            }
        }
        if (nivel3)
        {
            string filePath3 = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "HighScore3.json");

            if (File.Exists(filePath3))
            {
                string jsonData = File.ReadAllText(filePath3);

                Score data = JsonUtility.FromJson<Score>(jsonData);

                HighScore3 = data.HighScores3;

                Debug.Log("Puntaje máximo cargado correctamente.");
            }
            else
            {
                //Debug.Log("No se encontró el archivo de puntaje máximo.");
            }
        }

    }

    public void GuardarHabilidades()
    {
        HabilidadesData data = new HabilidadesData();
        data.habilidades = arbolHabilidadesSI.habilidadescargadas;

        string jsonData = JsonConvert.SerializeObject(data);

        string filePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "habilidades.json");
        File.WriteAllText(filePath, jsonData);

        Debug.Log("Habilidades guardadas correctamente.");
    }

    public void CargarHabilidades()
    {
        string filePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "habilidades.json");

        if (File.Exists(filePath))
        {
            string jsonData = File.ReadAllText(filePath);

            HabilidadesData data = JsonConvert.DeserializeObject<HabilidadesData>(jsonData);

            HabilidadesCargadas = data.habilidades;
            HabilidadesCargadas = HabilidadesCargadas
            .GroupBy(h => h.nombre)
            .Select(g => g.First())
            .ToList();
            arbolHabilidadesSI.habilidadescargadas = data.habilidades;

            Debug.Log("Habilidades cargadas correctamente.");
        }
        else
        {
            //Debug.Log("No se encontró el archivo de habilidades.");
        }
    }
    public void BorrarHabilidades()
    {
        string filePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "habilidades.json");

        string HighScore1 = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "HighScore1.json");
        string HighScore2 = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "HighScore2.json");
        string HighScore3 = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "HighScore3.json");

        string JefesData = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "JefesData.json");
        if (File.Exists(filePath))
        {
            File.Delete(filePath);
            Debug.Log("Habilidades borradas correctamente.");
        }
        else
        {
            Debug.Log("No se encontró el archivo de habilidades.");
        }
        if (File.Exists(HighScore1))
        {
            File.Delete(HighScore1);
            Debug.Log("HighScore borradas correctamente.");
        }
        else
        {
            Debug.Log("No se encontró el archivo de HighScore.");
        }
        if (File.Exists(HighScore2))
        {
            File.Delete(HighScore2);
            Debug.Log("HighScore borradas correctamente.");
        }
        else
        {
            Debug.Log("No se encontró el archivo de HighScore.");
        }
        if (File.Exists(HighScore3))
        {
            File.Delete(HighScore3);
            Debug.Log("HighScore borradas correctamente.");
        }
        else
        {
            Debug.Log("No se encontró el archivo de HighScore.");
        }
        if (File.Exists(JefesData))
        {
            File.Delete(JefesData);
            Debug.Log("JefesData borradas correctamente.");
        }
        else
        {
            Debug.Log("No se encontró el archivo de JefesData.");
        }
    }
}

