using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class MenuManager : MonoBehaviour
{
    public GameObject FondoSpaceImpact;
    public GameObject _CargarPartida;
    public GameObject InstruccionesImg;
    public GameObject OptionsImg;
    public Button Nivel1, Nivel2, Nivel3;
    public Slider slider;
    public float slidervolumen;
    public TMPro.TMP_Text HighScore1, HighScore2, HighScore3;
    public HabilidadesManager habilidadesmanager;
    public BDManager _BDManager;

    public TMPro.TMP_InputField UINombre;
    public void Start()
    {
        InstruccionesImg.SetActive(false);
        OptionsImg.SetActive(false);
        slider.value = PlayerPrefs.GetFloat("volumenAudio", 0.5f);
        AudioListener.volume = slider.value;

        


    }
    private void Update()
    {
        HighScore1.text = habilidadesmanager.HighScore1.ToString("0000000");
        HighScore2.text = habilidadesmanager.HighScore2.ToString("0000000");
        HighScore3.text = habilidadesmanager.HighScore3.ToString("0000000");
        if (habilidadesmanager.Jefe1Muerto)
        {
            Nivel2.interactable = true;
        }
        else
        {
            Nivel2.interactable = false;
        }
        if (habilidadesmanager.Jefe2Muerto)
        {
            Nivel3.interactable = true;
        }
        else
        {
            Nivel3.interactable = false;
        }


    }
    public void _Nivel1()
    {
        SceneManager.LoadScene("Nivel1");
    }
    public void _Nivel2()
    {
        SceneManager.LoadScene("Nivel2");
    }
    public void _Nivel3()
    {
        SceneManager.LoadScene("Nivel3");
    }
    public void Play()
    {
        habilidadesmanager.BorrarHabilidades();
        habilidadesmanager.NombreDelUsuario = UINombre.text;

        _BDManager.CreateUser();
        

        //SceneManager.LoadScene("Nivel1");


    }
    public void Salir()
    {
        Application.Quit();
    }
    public void Instrucciones()
    {
        InstruccionesImg.SetActive(true);
        FondoSpaceImpact.SetActive(false);



    }
    public void CargarPartida()
    {
        _CargarPartida.SetActive(true);
        FondoSpaceImpact.SetActive(false);
        InstruccionesImg.SetActive(false);
        OptionsImg.SetActive(false);

    }
    public void Back()
    {
        FondoSpaceImpact.SetActive(true);
        InstruccionesImg.SetActive(false);
        OptionsImg.SetActive(false);


    }
    public void Atras()
    {
        _CargarPartida.SetActive(false);
        FondoSpaceImpact.SetActive(true);
        InstruccionesImg.SetActive(false);
        OptionsImg.SetActive(false);
    }
    public void OpcionesButton()
    {
        OptionsImg.SetActive(true);
        FondoSpaceImpact.SetActive(false);
    }
    public void Opciones(float pvalor)
    {
        slidervolumen = pvalor;
        PlayerPrefs.SetFloat("volumenAudio", slidervolumen);
        AudioListener.volume = slider.value;

    }

}
