using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoEstadoBase 
{
    public string nombre;
    public float Velocidad { get; set; }
    protected FSM _m_e;
    public EnemigoEstadoBase(string nombre,FSM _m_e)
    {
        this.nombre = nombre;
        this._m_e = _m_e;
    }
    public virtual void Inicio(){}
    public virtual void Actualizar(){}
    public virtual void Terminar(){}


}
