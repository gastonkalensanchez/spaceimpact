using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IDLE : EnemigoEstadoBase
{
    MoverEnemigos m_e;
    public IDLE(MoverEnemigos _m_e):base("HabitanteIDLE",_m_e)
    {
        m_e = _m_e;
    }

    private Vector3 _puntoSpawn;
    int PuntoSpawn;
    

    public override void Inicio()
    {
        base.Inicio();
        PuntoSpawn = Random.Range(0,m_e._puntosSpawn.Length);
        m_e._llegoAlPunto = false;
        m_e.PuedeMorir = false;
        m_e.rb.constraints = RigidbodyConstraints.None|RigidbodyConstraints.FreezeRotation|RigidbodyConstraints.FreezePositionY;
    }
    public override void Actualizar()
    {
        base.Actualizar();  
        if (m_e._llegoAlPunto && m_e.tag == "AlienCongelado")
        {
            m_e.CambiarEstado(m_e._alienCongelado_Correr);
            return;
        }
        if (m_e._llegoAlPunto && m_e.tag == "HabitanteKepSpace")
        {
            m_e.CambiarEstado(m_e._habitante_correr);
            return;
        }
        if (m_e._llegoAlPunto && m_e.tag == "AlienEspacio")
        {
            m_e.CambiarEstado(m_e._alienEspacio_Correr);
            return;
        }
        if (m_e._llegoAlPunto && m_e.tag == "NaveEspacio")
        {
            m_e.CambiarEstado(m_e._naveEspacio_Correr);
            return;
        }
        if (m_e._llegoAlPunto && m_e.tag == "NaveTierra")
        {
            m_e.CambiarEstado(m_e._naveTierra_Correr);
            return;
        }
        if (m_e._llegoAlPunto && m_e.tag == "AlienTierra")
        {
            m_e.CambiarEstado(m_e._alienTierra_Correr);
            return;
        }
        if (!(m_e._llegoAlPunto))
        {
            
            if (m_e.tag == "NaveEspacio")
            {
                _puntoSpawn = (m_e)._puntosSpawn[PuntoSpawn].position;
                m_e.transform.position = new Vector3(m_e.transform.position.x,0,m_e.transform.position.z); 
                m_e.transform.position = Vector3.MoveTowards(m_e.transform.position,_puntoSpawn,0.5f);
                return;
            }else
            {
                _puntoSpawn = (m_e)._puntosSpawn[PuntoSpawn].position;
                m_e.transform.position = new Vector3(m_e.transform.position.x,-2,m_e.transform.position.z); 
                m_e.transform.position = Vector3.MoveTowards(m_e.transform.position,_puntoSpawn,0.5f);
                return;
            }
            
            
           
        }
        
            
          


        

    }
    public override void Terminar()
    {
        base.Terminar();
        m_e.PuedeMorir = true;
    }

    
 
}
