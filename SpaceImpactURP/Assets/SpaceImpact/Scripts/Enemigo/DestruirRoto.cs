using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestruirRoto : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Destruir());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    IEnumerator Destruir()
    {
        yield return new WaitForSecondsRealtime(0.5f);
        Destroy(this.gameObject);
    }
}
