using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MoverEnemigos : FSM
{
    public GameObject Particulas;
    public IDLE _Idle;
    public AlienCongelado_Correr _alienCongelado_Correr;
    public AlienCongelado_Ataque _alienCongelado_Ataque;
    public Habitante_CORRER _habitante_correr;
    public Habitante_Ataque _habitante_ataque;
    public Transform[] _puntosSpawn;
    public bool _llegoAlPunto;
    public NavMeshAgent _navMeshAgent;
    public GameObject _Jugador;
    public GameObject _Enemigos;
    public float tiempoInicio;

    [Header ("Nivel 1")]
    public Transform PuntoDeDisparo;
    public GameObject Proyectil;
    internal Rigidbody rb;
    public bool PuedeMorir;

    [Header("Nivel 2")]
    public bool auxmuertelvl2;
    public AlienEspacio_CORRER _alienEspacio_Correr;
    public NaveEspacio_Correr _naveEspacio_Correr;
    public Transform[] Naves;
    public Transform[] PuntosNave;

    [Header ("Nivel 3")]
    public NaveTierra_Corr _naveTierra_Correr;
    public AlienTierra_Correr _alienTierra_Correr;
    public Transform[] NaveTierraPuntos;
    public Transform NaveTierra;
    public Transform PuntoDeDisparoNaveTierra;

    #region animaciones
    public Animator animator;
    private string estado;
    #endregion
    #region Muerte
    public GameObject _ModeloRoto;
    public bool _Murio;
    #endregion

    float AuxiliarNoLlegoalpunto;
    float AuxiliarLlegoalpunto;

    // Start is called before the first frame update
    void Awake() 
    {
        _Idle = new IDLE(this);   
        _alienCongelado_Correr = new AlienCongelado_Correr(this);
        _alienCongelado_Ataque = new AlienCongelado_Ataque(this);
        _habitante_correr = new Habitante_CORRER(this);
        _habitante_ataque = new Habitante_Ataque(this);

        _alienEspacio_Correr = new AlienEspacio_CORRER(this);
        _naveEspacio_Correr = new NaveEspacio_Correr(this);

        _naveTierra_Correr = new NaveTierra_Corr(this);
        _alienTierra_Correr = new AlienTierra_Correr(this);

    }
    void Start()
    {
        if (GameObject.FindGameObjectWithTag("GameController"))
        {
            _puntosSpawn = GameControllerSI._gameControllerLvL1.Enemigos_puntosSpawn;
            _Jugador = GameControllerSI._gameControllerLvL1.Jugador; 
            _Enemigos = GameControllerSI._gameControllerLvL1._Enemigos_PuntosSpawn;
        }

        tiempoInicio = Time.time;
        if (GetComponent<Rigidbody>())
        {
            rb = GetComponent<Rigidbody>();
        }
        
        Comenzar();
        if (GetComponent<NavMeshAgent>())
        {
            _navMeshAgent = GetComponent<NavMeshAgent>();
        }  
        
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.y < -6)
        {
            Destroy(gameObject);
        }
        if(!_llegoAlPunto)
        {
            AuxiliarNoLlegoalpunto += Time.deltaTime;
        }
        if (AuxiliarNoLlegoalpunto >= 20)
        {
            Destroy(gameObject);
            AuxiliarNoLlegoalpunto = 0;
        }
        if (_llegoAlPunto)
        {
            AuxiliarLlegoalpunto += Time.deltaTime;
        }
        if (AuxiliarLlegoalpunto >= 30)
        {
            Destroy(gameObject);
            AuxiliarLlegoalpunto = 0;
        }
    }
    void FixedUpdate() 
    {
        if (!GameControllerSI._gameControllerLvL1.PausarJuego)
        {
            Actualizar();
        }
        
    }
    public void cambiarestadoanimacion(string nuevoestado)
    {
        if (estado == nuevoestado)
        {
            return;
        }
        animator.Play(nuevoestado);
        estado = nuevoestado;
    }
    protected override EnemigoEstadoBase IniciarEstado()
    {
        _Idle.Inicio();
        return _Idle;
    }
    public bool EstaCercaDelJugador()
    {
        if (Vector3.Distance(this.gameObject.transform.position,_Jugador.transform.position) < 30)
        {
            return true;
        }
        return false;

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PuntoSpawn"))
        {
            _llegoAlPunto = true;
        }
        if (other.gameObject.CompareTag("Proyectil"))
        {
            Instantiate(Particulas,transform.position,Quaternion.identity);
            Destroy(other.gameObject);
            RecibiDaño();
        }
        
    }
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Proyectil"))
        {
            Instantiate(Particulas,transform.position,Quaternion.identity);
            Destroy(other.gameObject);
            RecibiDaño();
        }
    }
    public void RecibiDaño()
    {       
        if (PuedeMorir)
        {
            var rempazar = Instantiate(_ModeloRoto,transform.position,Quaternion.identity);             
            var rbs = rempazar.GetComponentsInChildren<Rigidbody>();
            foreach (var item in rbs)
            {
                item.AddExplosionForce(1000,transform.position,500f);
            }
            Destroy(this.gameObject);
    
        }
        

    }
    public void Disparar(Transform pPuntoDeDisparo)
    {
        transform.LookAt(_Jugador.transform.position);
        GameObject p = Instantiate(Proyectil,pPuntoDeDisparo.transform.position,pPuntoDeDisparo.rotation);
        p.GetComponent<Rigidbody>().AddForce(pPuntoDeDisparo.transform.forward * 50,ForceMode.Impulse);
        Destroy(p, 5f);
        return;

    }
    public void DispararNave(Transform pPuntoDeDisparo)
    {
        foreach (Transform item in PuntosNave)
        {

            if (item != null)
            {
                item.transform.LookAt(_Jugador.transform.position);
            }
            
        }
        
        GameObject p = Instantiate(Proyectil,pPuntoDeDisparo.transform.position,pPuntoDeDisparo.rotation);
        p.GetComponent<Rigidbody>().AddForce(pPuntoDeDisparo.transform.forward * 50,ForceMode.Impulse);
        Destroy(p, 5f);
        return;

    }



}
