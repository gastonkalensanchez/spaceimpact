using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explocion : MonoBehaviour
{
    private int Cubos = 4;
    private float delay = 0f;
    private float fuerza = 1000f;
    private float radios = 500f;
  

 
    IEnumerator Main()
    {
        yield return new WaitForSecondsRealtime(delay);
        for (int x = 0; x < Cubos; x++)
        {
            for (int y = 0; y < Cubos; y++)
            {
                for (int z = 0; z < Cubos; z++)
                {
                    crearcubo(new Vector3(x, y, z));
                }
            }
        } // creamos cubitos en el cubo
        Destroy(gameObject); //eliminamos el original
        
    }
    void crearcubo(Vector3 cordenadas) //creamos el mismo cubo pero modificado
    {
        GameObject cubos = GameObject.CreatePrimitive(PrimitiveType.Cube);
        Renderer rd = cubos.GetComponent<Renderer>();
        rd.material = GetComponent<Renderer>().material;

        cubos.transform.localScale = transform.localScale; 

        Vector3 primercubo = transform.position - transform.localScale / 2 + cubos.transform.localScale / 2; 

        cubos.transform.position = primercubo + Vector3.Scale(cordenadas, cubos.transform.localScale); 

        Rigidbody rb = cubos.AddComponent<Rigidbody>();         
        
        rb.AddExplosionForce(fuerza, transform.position, radios);
        cubos.gameObject.tag = "Proyectil";
        cubos.gameObject.GetComponent<BoxCollider>().isTrigger = true;
        Destroy(cubos, 2);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Estructura"))
        {
            StartCoroutine(Main());
            Destroy(other.gameObject);
        }
        if (other.name.Substring(0, 3).Equals("ENE"))
        {
            StartCoroutine(Main());
            Destroy(other.gameObject);
        }


    }

}
