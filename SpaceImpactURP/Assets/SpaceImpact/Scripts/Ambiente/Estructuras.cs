using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Estructuras : MonoBehaviour
{
    public bool farola;
    private void Start() 
    {
        if (farola)
        {
            transform.position = new Vector3(transform.position.x,transform.position.y - 1,transform.position.z);
        }
    }
    void Update()
    {
        if (!GameControllerSI._gameControllerLvL1.PausarJuego)
        {
            if (GameObject.FindGameObjectWithTag("Player").GetComponent<Jugador>().Direccion == 0)
            {
                transform.Translate(0,0, 100 * Time.deltaTime);
            }
            if (GameObject.FindGameObjectWithTag("Player").GetComponent<Jugador>().Direccion == 1)
            {
                transform.Translate(20 * Time.deltaTime,0, 100 * Time.deltaTime);
            }
            if (GameObject.FindGameObjectWithTag("Player").GetComponent<Jugador>().Direccion == -1)
            {
                transform.Translate(-20 * Time.deltaTime,0, 100 * Time.deltaTime);
            }     

        }
        
    }


}
