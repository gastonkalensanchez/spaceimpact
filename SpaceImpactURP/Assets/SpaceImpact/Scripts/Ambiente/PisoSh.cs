using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PisoSh : MonoBehaviour
{
    public Material material; // Asigna el material que utiliza el shader en el inspector
    public float MovimientoY,AuxiliarY;
    public float MovimientoX,AuxiliarX;
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        MovimientoY += AuxiliarY * Time.deltaTime;
        if (GameObject.FindGameObjectWithTag("Player").GetComponent<Jugador>().Direccion == 0 && !GameControllerSI._gameControllerLvL1.PausarJuego)
        {
            MovimientoX += 0 * Time.deltaTime;
        }
        if (GameObject.FindGameObjectWithTag("Player").GetComponent<Jugador>().Direccion == 1 && !GameControllerSI._gameControllerLvL1.PausarJuego)
        {
            MovimientoX -= AuxiliarX * Time.deltaTime;
        }
        if (GameObject.FindGameObjectWithTag("Player").GetComponent<Jugador>().Direccion == -1 && !GameControllerSI._gameControllerLvL1.PausarJuego)
        {
            MovimientoX += AuxiliarX * Time.deltaTime;
        }  
        if (!GameControllerSI._gameControllerLvL1.PausarJuego)
        {
            material.SetFloat("_MovimientoY", MovimientoY );
            material.SetFloat("_MovimientoX", MovimientoX);
        }


    }
}
