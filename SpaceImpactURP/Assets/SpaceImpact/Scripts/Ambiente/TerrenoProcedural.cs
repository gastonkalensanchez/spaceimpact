using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrenoProcedural : MonoBehaviour
{

    public GameObject[] ObjetosTierra;
    public GameControllerSI gameControllerSI;
    private GameObject Jugador;

    [Range(0f,1f)]
    public float pro = 0.75f;
    float tiempoaux;

    void Start()
    {
        Jugador = GameObject.FindGameObjectWithTag("Player");
        gameControllerSI = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameControllerSI>();

    }
    
    void Update() {

        if (gameControllerSI.PausarJuego == false)
        {
            if (!gameControllerSI.BossFight)
            {
                tiempoaux += Time.deltaTime;
                if (tiempoaux >= 2)
                {
                    if (Random.Range(0f, 1f) <= pro)
                    {
                        Instantiate
                        (
                            ObjetosTierra[Random.Range(0, ObjetosTierra.Length)],
                            transform.position,
                            Quaternion.Euler(0,180,0)
                        );
                    }
                    tiempoaux = 0;
                }
            }
 
        }
    }

}
