using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fondo : MonoBehaviour
{
    [SerializeField] private Vector2 velocidad;
    public Material material;
    public Vector2 offset;
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (GameObject.FindGameObjectWithTag("Player").GetComponent<Jugador>().Direccion == 1)
        {
            offset = velocidad * Time.deltaTime;
            material.mainTextureOffset += offset;        
        }
        if (GameObject.FindGameObjectWithTag("Player").GetComponent<Jugador>().Direccion == -1)
        {
            offset = velocidad * Time.deltaTime;
            material.mainTextureOffset -= offset;
        }  


    }
    
}
