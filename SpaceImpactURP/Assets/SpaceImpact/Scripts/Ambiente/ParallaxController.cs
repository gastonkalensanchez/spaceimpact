
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxController : MonoBehaviour
{

    public float ve1locidad = 0.1f;
    private GameObject Jugador;
    private GameObject Fondo;
    private GameObject Terreno;
    private GameObject piso;
    private GameObject[] Estructuras;
    private Rigidbody rb;
    private Vector3 posiciondelpisoinicial;
    public float LugarDeReinicio;

    void Start()
    {
        CargarGameObjets();
 

    }
    void Update()
    {
        ControlDeDistancia();
        SeguirJugador();

    
        
    }

    void ControlDeDistancia()
    {
        if (piso != null)
        {
            if (piso.transform.position.z <= LugarDeReinicio)
            {
                piso.transform.position = posiciondelpisoinicial;
            }
        }
        
      
        
    }
    void CargarUbicacionInicial()
    {
        posiciondelpisoinicial = new Vector3(piso.transform.position.x, piso.transform.position.y, piso.transform.position.z);
    }

    void SeguirJugador()
    {
        if (Fondo != null)
        {
            Fondo.transform.position = new Vector3(-Jugador.transform.position.x,Fondo.transform.position.y,Fondo.transform.position.z);
        }
        if (Terreno != null)
        {
            Terreno.transform.position = new Vector3(Jugador.transform.position.x,Terreno.transform.position.y,Terreno.transform.position.z);
        }
        
        
    }
    void CargarGameObjets()
    {
        if (Jugador == null)
        {
            Jugador = GameObject.FindGameObjectWithTag("Player");
        }
        if (Terreno == null)
        {
            Terreno = GameObject.FindGameObjectWithTag("Terreno");
        }
        if (Fondo == null)
        {
            Fondo = GameObject.FindGameObjectWithTag("Fondo");
        }
        if (piso == null)
        {
            piso = GameObject.FindGameObjectWithTag("Piso");
            rb = piso.GetComponent<Rigidbody>();
            rb.velocity = new Vector3(0, 0, 0);   
            CargarUbicacionInicial(); 
        }
       
        
       

    }

}
