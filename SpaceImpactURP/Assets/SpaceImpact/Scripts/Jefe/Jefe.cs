using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jefe : MonoBehaviour
{
    public GameObject Particulas;
    public GameObject _Jefe;
    public int JefeVida; 
    public Transform puntodisparo;
    public GameObject _Proyectil;
    public GameObject _Jugador;
    public float auxdisparo;
    public GameObject _ModeloRoto;
    public GameObject Canvas;
    public bool PuedeDisparar;

    public List<GameObject> Naves;
    public bool lvl3;
    void Start()
    {
        JefeVida = 20;
        _Jugador = GameObject.FindGameObjectWithTag("Player");
        Canvas = GameObject.FindGameObjectWithTag("Canvas");
    }
    
    
    void Update()
    {


        if (!lvl3)
        {
            puntodisparo.LookAt(_Jugador.transform.position);
            auxdisparo += Time.deltaTime;
            if (auxdisparo >= 0.8f && PuedeDisparar)
            {

                Disparar(puntodisparo);
                auxdisparo = 0;
            }
        }

        Naves.RemoveAll(item => item == null);
        if (Naves.Count <=0 && lvl3)
        {
            JefeVida = 0;
            RecibirDaño();
        }
    }
    public void Disparar(Transform pPuntoDeDisparo)
    {

        GameObject p = Instantiate(_Proyectil,pPuntoDeDisparo.transform.position,pPuntoDeDisparo.rotation);
        p.GetComponent<Rigidbody>().AddForce(pPuntoDeDisparo.transform.forward * 50,ForceMode.Impulse);
        Destroy(p, 5f);
        

    }

    public void RecibirDaño()
    {
        if (JefeVida <= 0)
        {
            var rempazar = Instantiate(_ModeloRoto,transform.position,Quaternion.identity);             
            var rbs = rempazar.GetComponentsInChildren<Rigidbody>();
            foreach (var item in rbs)
            {
                item.AddExplosionForce(1000,transform.position,500f);
            }
            if (GameControllerSI._gameControllerLvL1._Nivel1)
            {
                GameControllerSI._gameControllerLvL1.habilidadesManager.Jefe1Muerto = true;
                GameControllerSI._gameControllerLvL1.habilidadesManager.GuardarVariablesJefes();
            }
            if (GameControllerSI._gameControllerLvL1._Nivel2)
            {
                GameControllerSI._gameControllerLvL1.habilidadesManager.Jefe2Muerto = true;
                GameControllerSI._gameControllerLvL1.habilidadesManager.GuardarVariablesJefes();
            }
            if (GameControllerSI._gameControllerLvL1._Nivel3)
            {
                GameControllerSI._gameControllerLvL1.habilidadesManager.Jefe3Muerto = true;
                GameControllerSI._gameControllerLvL1.habilidadesManager.GuardarVariablesJefes();
            }
            GameControllerSI._gameControllerLvL1.MatoAlJefe = true;
            GameControllerSI._gameControllerLvL1.habilidadesManager.GuardarHabilidades();
            GameControllerSI._gameControllerLvL1.habilidadesManager.GuardarPuntajeMaximo();
            Destroy(_Jefe);
        }
        else
        {
            JefeVida -= 1;
        }

    }
    private void OnCollisionEnter(Collision other) 
    {
        if (other.gameObject.CompareTag("Proyectil"))
        {
            GameObject par = Instantiate(Particulas, transform.position, Quaternion.identity);
            par.transform.localScale = new Vector3(3, 3, 3);

            Destroy(other.gameObject);
            RecibirDaño();
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Proyectil"))
        {
            GameObject par = Instantiate(Particulas, transform.position, Quaternion.identity);
            par.transform.localScale = new Vector3(3, 3, 3);

            Destroy(other.gameObject);
            RecibirDaño();
        }
    }

}
