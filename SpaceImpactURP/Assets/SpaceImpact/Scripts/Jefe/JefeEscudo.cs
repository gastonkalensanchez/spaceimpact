using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JefeEscudo : MonoBehaviour
{
    public GameObject Particulas;
    private void OnCollisionEnter(Collision other) 
    {
        if (other.gameObject.CompareTag("Proyectil"))
        {
            Instantiate(Particulas,transform.position,Quaternion.identity);
            Destroy(other.gameObject);
        }
        
    }
}
