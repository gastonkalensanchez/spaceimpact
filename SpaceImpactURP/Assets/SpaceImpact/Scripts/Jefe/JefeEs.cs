using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JefeEs : MonoBehaviour
{
    public GameObject Particulas;
    public int vidajefees = 10;
    private void Update() {
        if (vidajefees <= 0)
        {
            gameObject.SetActive(false);
        }   
        
    }
    private void OnCollisionEnter(Collision other) 
    {
        if (other.gameObject.CompareTag("Proyectil"))
        {
            Instantiate(Particulas,transform.position,Quaternion.identity);
            Destroy(other.gameObject);
            vidajefees -= 1;
        }
        
    }
}   
