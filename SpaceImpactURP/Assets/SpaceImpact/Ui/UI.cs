using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class UI : MonoBehaviour
{
    public GameControllerSI _gamecontroller;
    public Jugador _Jugador;
    public JugadorEstetica _JugadorEstetica;
    public ArbolHabilidadesSI _arboldehabilidades;
    public GameObject Vida,Vida1,Vida2;
    public GameObject Habilidad,Habilidad1,Habilidad2;
    public TMP_Text Score;
    public float MaximoH1,BarraH1,MaximoH2,BarraH2, MaximoH3, BarraH3, MaximoBoss, BarraBoss;
    public Image ImageH1,ImageH2,ImageH3;
    public Slider BossProgreso,BossLife;

    public TMP_Text ScoreMax;
    public HabilidadesManager _habilidadesManager;
    public GameObject bcontinuar;
    public Image H1, H2, H3;
    void Update()
    {
        if (_gamecontroller.MatoAlJefe)
        {
            _gamecontroller.PausarJuego = true;
            bcontinuar.SetActive(true);

        }
        else
        {
            bcontinuar.SetActive(false);

        }
        if (Score != null)
        {
            if (_gamecontroller._Nivel1)
            {
                Score.text = ScoreManager.Scoree1.ToString("0000000");
                ScoreMax.text = _habilidadesManager.HighScore1.ToString("0000000");
            }
            if (_gamecontroller._Nivel2)
            {
                Score.text = ScoreManager.Scoree2.ToString("0000000");
                ScoreMax.text = _habilidadesManager.HighScore2.ToString("0000000");
            }
            if (_gamecontroller._Nivel3)
            {
                Score.text = ScoreManager.Scoree3.ToString("0000000");
                ScoreMax.text = _habilidadesManager.HighScore3.ToString("0000000");
            }

        }
        
        MoastrarVidaHabilidad();
        _Barra();        
    }
    public void Creditos()
    {
        SceneManager.LoadScene("Creditos");
        _habilidadesManager.GuardarPuntajeMaximo();
    }
    public void Continuar()
    {
        _Jugador.Continuar.SetActive(false);
        _gamecontroller.PausarJuego = false;
        _gamecontroller.MatoAlJefe = false;
        _habilidadesManager.GuardarPuntajeMaximo();
        if (_gamecontroller._Nivel1)
        {
            _gamecontroller.Nivel1Sound.Play();
            _gamecontroller.BossfightSound.Stop();
        }
        if (_gamecontroller._Nivel2)
        {
            _gamecontroller.Nivel2Sound.Play();
            _gamecontroller.BossfightSound.Stop();

        }
        if (_gamecontroller._Nivel3)
        {
            _gamecontroller.Nivel3Sound.Play();
            _gamecontroller.BossfightSound.Stop();
        }

    }
    public void Nivel1()
    {
        if (_Jugador.Murio)
        {
            ScoreManager.Scoree1 = 0;
        }
        ScoreManager.Scoree2 = 0;
        ScoreManager.Scoree3 = 0;
        SceneManager.LoadScene("Nivel1");
    }
    public void Nivel2()
    {
        if (_Jugador.Murio)
        {
            ScoreManager.Scoree2 = 0;
        }
        ScoreManager.Scoree1 = 0;
        ScoreManager.Scoree3 = 0;
        SceneManager.LoadScene("Nivel2");
    }
    public void Nivel3()
    {
        if (_Jugador.Murio)
        {
            ScoreManager.Scoree3 = 0;
        }
        ScoreManager.Scoree1 = 0;
        ScoreManager.Scoree2 = 0;
        SceneManager.LoadScene("Nivel3");
    }
    public void Menu()
    {
        ScoreManager.Scoree1 = 0;
        ScoreManager.Scoree2 = 0;
        ScoreManager.Scoree3 = 0;

        SceneManager.LoadScene("Menu");
        _habilidadesManager.GuardarPuntajeMaximo();
    
    }
    void _Barra()
    {
        if (_arboldehabilidades.habilidadesEnMano.Count >= 1 && _arboldehabilidades.habilidadesEnMano[0] != null)
        {
            MaximoH1 = _arboldehabilidades.habilidadesEnMano[0].maximoenfriamiento;
            BarraH1 = _arboldehabilidades.habilidadesEnMano[0].actualenfriamiento;
            foreach (var im in _arboldehabilidades.imagenes)
            {
                if (im.name == _arboldehabilidades.habilidadesEnMano[0].imagenID)
                {
                    if (_arboldehabilidades.habilidadesEnMano[0].imagenID == "s_047" && _Jugador.TieneLaHabilidad(TipoDeHabilidad.PasivaCohete))
                    {
                        _JugadorEstetica.Cruz[0].SetActive(true);
                    }
                    H1.sprite = im;
                   
                }



            }


        }
        if (_arboldehabilidades.habilidadesEnMano.Count >= 2 && _arboldehabilidades.habilidadesEnMano[1] != null)
        {
            MaximoH2 = _arboldehabilidades.habilidadesEnMano[1].maximoenfriamiento;
            BarraH2 = _arboldehabilidades.habilidadesEnMano[1].actualenfriamiento;
            foreach (var im in _arboldehabilidades.imagenes)
            {
                if (im.name == _arboldehabilidades.habilidadesEnMano[1].imagenID)
                {
                    if (_arboldehabilidades.habilidadesEnMano[1].imagenID == "s_047" && _Jugador.TieneLaHabilidad(TipoDeHabilidad.PasivaCohete))
                    {
                        _JugadorEstetica.Cruz[1].SetActive(true);
                    }
                    H2.sprite = im;


                }


            }
        }
        if (_arboldehabilidades.habilidadesEnMano.Count >= 3 && _arboldehabilidades.habilidadesEnMano[2] != null)
        {
            MaximoH3 = _arboldehabilidades.habilidadesEnMano[2].maximoenfriamiento;
            BarraH3 = _arboldehabilidades.habilidadesEnMano[2].actualenfriamiento;
            foreach (var im in _arboldehabilidades.imagenes)
            {
                if (im.name == _arboldehabilidades.habilidadesEnMano[2].imagenID)
                {
                    if (_arboldehabilidades.habilidadesEnMano[2].imagenID == "s_047" && _Jugador.TieneLaHabilidad(TipoDeHabilidad.PasivaCohete))
                    {
                        _JugadorEstetica.Cruz[2].SetActive(true);
                    }   
                    H3.sprite = im;


                }

   
            }

        }
        if (!_gamecontroller.BossFight)
        {
            BossLife.gameObject.SetActive(false);
            BossProgreso.gameObject.SetActive(true);
            MaximoBoss = 100;
            BarraBoss = GameControllerSI._gameControllerLvL1.AuxBossFight;
            float FILLBOSS = (float)BarraBoss / (float)MaximoBoss;
            BossProgreso.value = FILLBOSS;
        }
        else
        {
            BossProgreso.gameObject.SetActive(false);
            BossLife.gameObject.SetActive(true);
        }    

        float FILLH1 = (float)BarraH1 / (float)MaximoH1;
        ImageH1.fillAmount = FILLH1;
        float FILLH2 = (float)BarraH2 / (float)MaximoH2;
        ImageH2.fillAmount = FILLH2;
        float FILLH3 = (float)BarraH3 / (float)MaximoH3;
        ImageH3.fillAmount = FILLH3;

    }
    void MoastrarVidaHabilidad()
    {
        if (_Jugador.vida == 3)
        {
            Vida.gameObject.SetActive(true);
            Vida1.gameObject.SetActive(true);
            Vida2.gameObject.SetActive(true);
        }
        if (_Jugador.vida == 2)
        {
            Vida.gameObject.SetActive(true);
            Vida1.gameObject.SetActive(true);
            Vida2.gameObject.SetActive(false);
        }
        if (_Jugador.vida == 1)
        {
            Vida.gameObject.SetActive(true);
            Vida1.gameObject.SetActive(false);
            Vida2.gameObject.SetActive(false);
        }
        if(_Jugador.vida == 0)
        {
            Vida.gameObject.SetActive(false);
            Vida1.gameObject.SetActive(false);
            Vida2.gameObject.SetActive(false);
        }
        if (_arboldehabilidades.habilidadesEnMano.Count == 0)
        {
            Habilidad.SetActive(false);
            Habilidad1.SetActive(false);
            Habilidad2.SetActive(false);
        }
        if (_arboldehabilidades.habilidadesEnMano.Count == 1 && _arboldehabilidades.habilidadesEnMano[0] != null)
        {

            Habilidad.SetActive(true);
            Habilidad1.SetActive(false);
            Habilidad2.SetActive(false);
        }
        if (_arboldehabilidades.habilidadesEnMano.Count == 2 && _arboldehabilidades.habilidadesEnMano[1] != null)
        {

            Habilidad.SetActive(true);
            Habilidad1.SetActive(true);
            Habilidad2.SetActive(false);
        }
        if (_arboldehabilidades.habilidadesEnMano.Count == 3 && _arboldehabilidades.habilidadesEnMano[2] != null)
        {

            Habilidad.SetActive(true);
            Habilidad1.SetActive(true);
            Habilidad2.SetActive(true);
        }
    }
}
