using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FondoNivel2 : MonoBehaviour
{

    bool instancio;
    private void Update()
    {
        // Mueve el objeto en la dirección negativa del eje Z
        transform.Translate(0,200 * Time.deltaTime,0);

        // Comprueba si el objeto ha llegado a la posición -1000 en Z
        if (this.transform.position.z <= 100f && !instancio)
        {
            instancio = true;
            // Instancia un nuevo objeto en la posición -800 en Z
            Instantiate(gameObject, new Vector3(transform.position.x, transform.position.y, 900f), Quaternion.Euler(-90,0,0));

            // Destruye el objeto actual
            
        }
        if (this.transform.position.z <=0)
        {
            Destroy(gameObject);
        }
    }

}
