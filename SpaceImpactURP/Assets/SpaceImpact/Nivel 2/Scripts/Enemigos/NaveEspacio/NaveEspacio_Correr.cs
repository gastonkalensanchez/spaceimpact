using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NaveEspacio_Correr : EnemigoEstadoBase
{
    MoverEnemigos m_e;
    public int CantidadDeNaves;
    public bool PuedeCaminar;
    public bool auxdisparo = true;
    public float auxCaminar;
    public NaveEspacio_Correr(MoverEnemigos _m_e):base("NaveEspacioCorrer",_m_e)
    {   
        m_e = (MoverEnemigos)_m_e;
    }
    public override void Inicio()
    {
        base.Inicio();
        
        m_e.PuedeMorir = false;
        m_e.transform.position = new Vector3(m_e.transform.position.x + Random.Range(1,5),0.5f,m_e.transform.position.z);
        m_e.GetComponent<CapsuleCollider>().isTrigger = true;
        m_e.auxmuertelvl2 = true;
    }
    public override void Actualizar()
    {
        base.Actualizar();      
        if ((m_e)._llegoAlPunto)
        {
            MovNaveEspacio();
            return;
        }


    }
    public override void Terminar()
    {
        base.Terminar();
    }
    void MovNaveEspacio()
    {
        if (m_e.transform.childCount == 0)
        {
            m_e.PuedeMorir = true;
            m_e.RecibiDaño();
        }
        if (!PuedeCaminar)
        {
            m_e.StartCoroutine(Disparo());
            auxdisparo = false;
        }
        else
        {
            
            auxCaminar += Time.deltaTime;
            m_e.GetComponent<Rigidbody>().MovePosition(new Vector3(m_e.transform.position.x,m_e.transform.position.y,m_e.transform.position.z - 20f * Time.deltaTime));
            if (auxCaminar > Random.Range(2,4))
            {
                auxdisparo = true;
                PuedeCaminar = false;
            }
            
        }

    }
    IEnumerator Disparo()
    {
        if(!m_e.EstaCercaDelJugador())
        {
            if (auxdisparo == true)
            {
                yield return new WaitForSecondsRealtime(Random.Range(1,2));
                foreach (var item in m_e.PuntosNave)
                {
                    if (item != null)
                    {
                        m_e.DispararNave(item);
                    }

                }   
                auxCaminar = 0;
                PuedeCaminar = true;
            }
        
        }

    }

    
    
}