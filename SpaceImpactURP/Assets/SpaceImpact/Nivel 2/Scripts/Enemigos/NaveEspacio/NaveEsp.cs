using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NaveEsp : MonoBehaviour
{
    public GameObject Particulas;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Proyectil") && GetComponentInParent<MoverEnemigos>().auxmuertelvl2)
        {
            Instantiate(Particulas,transform.position,Quaternion.identity);
            var rempazar = Instantiate(GetComponentInParent<MoverEnemigos>()._ModeloRoto, transform.position, Quaternion.identity);
            var rbs = rempazar.GetComponentsInChildren<Rigidbody>();
            foreach (var item in rbs)
            {
                item.AddExplosionForce(1000, transform.position, 500f);
            }
            Destroy(this.gameObject);
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Proyectil") && GetComponentInParent<MoverEnemigos>().auxmuertelvl2)
        {
            Instantiate(Particulas, transform.position, Quaternion.identity);
            var rempazar = Instantiate(GetComponentInParent<MoverEnemigos>()._ModeloRoto, transform.position, Quaternion.identity);
            var rbs = rempazar.GetComponentsInChildren<Rigidbody>();
            foreach (var item in rbs)
            {
                item.AddExplosionForce(1000, transform.position, 500f);
            }
            Destroy(this.gameObject);
        }
    }

}
