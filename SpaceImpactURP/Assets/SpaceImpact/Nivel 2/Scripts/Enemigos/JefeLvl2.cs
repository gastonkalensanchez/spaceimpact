using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JefeLvl2 : MonoBehaviour
{
    public GameObject JefeEs, JefeEs2;
    public GameObject JefeEscudo;
    public float aux;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (JefeEs.GetComponent<JefeEs>().vidajefees <= 0 && JefeEs2.GetComponent<JefeEs>().vidajefees <= 0)
        {
            GetComponent<Jefe>().PuedeDisparar = true;
            float FILLBOSS = GetComponent<Jefe>().JefeVida / (float)20;
            GetComponent<Jefe>().Canvas.GetComponent<UI>().BossLife.value = FILLBOSS;
            GameControllerSI._gameControllerLvL1.JefeCons.text = "Disparale al Jefe!";
            GetComponent<CapsuleCollider>().enabled = true;
            JefeEscudo.SetActive(false);
            aux += Time.deltaTime;
            if (aux >= 10)
            {
              JefeEscudo.SetActive(true);
              JefeEs.GetComponent<JefeEs>().vidajefees = 10;
              JefeEs2.GetComponent<JefeEs>().vidajefees = 10;
              JefeEs.SetActive(true);
              JefeEs2.SetActive(true);
              aux = 0;

            }
        }
        else
        {
            GetComponent<Jefe>().PuedeDisparar = false;
            float FILLBOSS = (JefeEs.GetComponent<JefeEs>().vidajefees + JefeEs2.GetComponent<JefeEs>().vidajefees) / (float)20;
            GetComponent<Jefe>().Canvas.GetComponent<UI>().BossLife.value = FILLBOSS;
            GameControllerSI._gameControllerLvL1.JefeCons.text = "Disparale a los 2 puntos!";
            GetComponent<CapsuleCollider>().enabled = false;
        }
    }
}
