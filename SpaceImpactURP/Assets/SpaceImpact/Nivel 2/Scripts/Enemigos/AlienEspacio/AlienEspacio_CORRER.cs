using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlienEspacio_CORRER : EnemigoEstadoBase
{
    MoverEnemigos m_e;
    public float amplitudey = 2f; // Amplitud del movimiento en arcoíris
    public float amplitudex = 9f;
    public float frequency = 2f; // Frecuencia del movimiento en arcoíris
    public float velocidad = 20f;
    private Vector3 initialPosition;
    public float distancia = 210f;
    public Vector3 nuevapos;
    public float PosIn;
    public AlienEspacio_CORRER(MoverEnemigos _m_e):base("AlienEspacioCORRER",_m_e)
    {   
        m_e = (MoverEnemigos)_m_e;
    }
    public override void Inicio()
    {
        base.Inicio();
        initialPosition = m_e.transform.position;
        initialPosition.z += Random.Range(0,10);
        m_e.PuedeMorir = true;
        m_e.tag = "EnemigoBala";
        
    }
    public override void Actualizar()
    {
        base.Actualizar();
        // Calcula la nueva posición en base al tiempo y los parámetros de amplitud y frecuencia
         
        float time = Time.time;
        float xOffset = Mathf.Sin(time * frequency) * amplitudex;
        float yOffset = Mathf.Cos(time * frequency) * amplitudey;
        float distance = Mathf.PingPong(time * velocidad,distancia) - 1f;
        nuevapos = new Vector3((PosIn + xOffset), yOffset , initialPosition.z - distance);

        m_e.transform.position = nuevapos;







    }
    public override void Terminar()
    {
        base.Terminar();
    }

    
    
}