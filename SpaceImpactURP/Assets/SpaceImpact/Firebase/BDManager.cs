using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Database;

public class BDManager : MonoBehaviour
{


    private string userID;
    private DatabaseReference dbReference;

    UsuariosP DataUsuario;

    void Start()
    {

        userID = SystemInfo.deviceUniqueIdentifier;
        dbReference = FirebaseDatabase.DefaultInstance.RootReference;
        Debug.Log(dbReference);
        Debug.Log(userID);
    }



    void Update()
    {
        
    }
    public void CreateUser() // creo el usuario 
    {
        if (dbReference == null)
        {
            Debug.LogError("dbReference is null. Firebase might not be initialized properly.");
            return;
        }
        Debug.Log("se creo usuario");
        UsuariosP newUser = new UsuariosP(HabilidadesManager._Persistencia.NombreDelUsuario, HabilidadesManager._Persistencia.HighScore1, HabilidadesManager._Persistencia.HighScore2, HabilidadesManager._Persistencia.HighScore3); // esta es una clase que cree que en su constructor recive eso 
        string json = JsonUtility.ToJson(newUser); // seriaizo el user con todos los datos a guardar asignados 
        dbReference.Child(HabilidadesManager._Persistencia.NombreDelUsuario).Child(userID).SetRawJsonValueAsync(json); // Esta linea crea el usuario en la base 

    }
    public IEnumerator GetUser() // primer paso
    {
        DataUsuario = new UsuariosP(); // instancio un nuevo usuario para guardar todos los datos que traiga de la base 
        var userData = dbReference.Child(HabilidadesManager._Persistencia.NombreDelUsuario).Child(userID).GetValueAsync(); // esta linea traigo los datos en base al nivel y nombre de ususario 

        yield return new WaitUntil(predicate: () => userData.IsCompleted); // que espere a que se carguen los datos

        if (userData != null)
        {
            DataSnapshot snapshot = userData.Result;

            // asigno los datos al user provisorio que cree 
            DataUsuario.Nombre = snapshot.Child("userName").Value.ToString();
            DataUsuario.PuntuacionLVL1 = int.Parse(snapshot.Child("Nivel1").Value.ToString());
            DataUsuario.PuntuacionLVL2 = int.Parse(snapshot.Child("Nivel2").Value.ToString());
            DataUsuario.PuntuacionLVL3 = int.Parse(snapshot.Child("Nivel3").Value.ToString());

            Debug.Log("se obtuvo la info del jugador " + DataUsuario.Nombre + " Nivel1 " + DataUsuario.PuntuacionLVL1 + " Nivel2 " + DataUsuario.PuntuacionLVL2 + " Nivel3 " + DataUsuario.PuntuacionLVL3);


        }
    }

}
