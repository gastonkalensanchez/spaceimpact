using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmbienteLv1 : MonoBehaviour
{
    public GameObject Montana;
    float tiempoaux;
    public float tiempospawn;
    void Start()
    {
        tiempospawn = 0.1f;
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameControllerSI._gameControllerLvL1.PausarJuego)
        {
            tiempoaux += Time.deltaTime;
            if (tiempoaux >= tiempospawn)
            {      
                GameObject nuevamontana = Instantiate
                    (
                        Montana,
                        transform.position,
                        Quaternion.identity
                    );

                nuevamontana.transform.SetParent(transform);
                tiempoaux = 0;
            }
        }


    }
}
