using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MontañaLV : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject[] montañas;
    void Start()
    {
        foreach (GameObject item in montañas)
        {
            item.transform.rotation = Quaternion.Euler(0, Random.Range(0, 90), 0); 
        }
       
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameControllerSI._gameControllerLvL1.PausarJuego)
        {
            transform.Translate(0, 0, -200 * Time.deltaTime);
        }
        
    }
}
