using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Habitante_Ataque : EnemigoEstadoBase
{
    MoverEnemigos m_e;
    public Habitante_Ataque(MoverEnemigos _m_e):base("HabitanteATAQUE",_m_e)
    {
        m_e = (MoverEnemigos)_m_e;
    }
    
    Rigidbody rb;
    float cont;
    public override void Inicio()
    {
        base.Inicio();
        rb = m_e.GetComponent<Rigidbody>();
        m_e.tag = "EnemigoBala";

    }
    public override void Actualizar()
    {
        base.Actualizar();  
        m_e.PuedeMorir = true;
        SaltoHaciaElJugador();
        return;

    }
    public override void Terminar()
    {
        base.Terminar();
    }

    void SaltoHaciaElJugador()
    {
        cont += Time.deltaTime;

        
        if (cont >=1)
        {
            
            m_e.transform.Translate(1 * Vector3.forward);
            m_e.cambiarestadoanimacion("HabitanteAtacando");
            
        }else
        { 
            m_e.transform.LookAt((m_e)._Jugador.transform);

        }
    }

}
