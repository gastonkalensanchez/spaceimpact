using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Habitante_CORRER : EnemigoEstadoBase
{
    MoverEnemigos m_e;
    public Habitante_CORRER(MoverEnemigos _m_e):base("HabitanteCORRER",_m_e)
    {
        m_e = (MoverEnemigos)_m_e;
    }
    
    public float velocidad = 20;
    public float zigzagAmplitud = 18;
    public float zigzagFrecuencia = 1;
    public bool salto = false;
    Rigidbody rb;



    public override void Inicio()
    {
        base.Inicio();
        rb = m_e.GetComponent<Rigidbody>();

    }
    public override void Actualizar()
    {
        base.Actualizar();  
        m_e.PuedeMorir = true;
        if((m_e).EstaCercaDelJugador())
        {
            m_e.CambiarEstado((m_e)._habitante_ataque);
            return;
        }
        if ((m_e)._llegoAlPunto)
        {
            MovimientoZicZac();
            return;
        }
        if (!(m_e)._llegoAlPunto)
        {
            m_e.CambiarEstado((m_e)._Idle);
            return;
        }
 
        

    }
    public override void Terminar()
    {
        m_e.transform.position = new Vector3(m_e.transform.position.x ,-2,m_e.transform.position.z);
        rb.constraints = RigidbodyConstraints.FreezeAll;
        base.Terminar();
    }
    void MovimientoZicZac()
    {

        // Calcula la dirección hacia el jugador en el eje z
        Vector3 direccionZ = (m_e)._Jugador.transform.position - m_e.transform.position;
        direccionZ.y = 0f;
        direccionZ.Normalize();

        // Calcula la dirección hacia el jugador en el eje x
        Vector3 direccionX = Vector3.right * direccionZ.z;

        // Calcula el desplazamiento zigzag
        float desplazamientoZigzag = Mathf.Sin((Time.time - (m_e).tiempoInicio) * zigzagFrecuencia) * zigzagAmplitud;

        // Calcula la nueva posición del enemigo
        Vector3 nuevaPosicion = m_e.transform.position;
        nuevaPosicion += direccionZ * velocidad * Time.deltaTime;
        nuevaPosicion.x = direccionX.x * desplazamientoZigzag;
        Vector3 direction = new Vector3(nuevaPosicion.x, 0f, -20f * Time.deltaTime);
        Quaternion rotation = Quaternion.LookRotation(-direction, Vector3.up);
        m_e.transform.rotation = rotation;
        m_e.cambiarestadoanimacion("HabitanteCorrer");
        // Actualiza la posición del enemigo
        //m_e.transform.position = nuevaPosicion;
        m_e.rb.MovePosition(nuevaPosicion);
        if (m_e.transform.position.x >= 12 && !salto)
        {
            
            rb.constraints = RigidbodyConstraints.None|RigidbodyConstraints.FreezeRotation;
            m_e.cambiarestadoanimacion("HabitanteSalto");
            rb.AddForce(Vector3.up * 15,ForceMode.Impulse);
            velocidad = 20;
            salto = true;
            (m_e).Disparar(m_e.PuntoDeDisparo);

        }

        if (m_e.transform.position.x <= -12 && salto)
        {
            m_e.cambiarestadoanimacion("HabitanteCorrer");
            rb.constraints = RigidbodyConstraints.None|RigidbodyConstraints.FreezeRotation|RigidbodyConstraints.FreezePositionY;
            velocidad = 0;
            salto = false;
            (m_e).Disparar(m_e.PuntoDeDisparo);
            m_e.transform.position = new Vector3(m_e.transform.position.x,-2,m_e.transform.position.z);
            
        }

    }
    





}
