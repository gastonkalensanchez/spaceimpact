using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlienCongelado_Ataque : EnemigoEstadoBase
{
    MoverEnemigos m_e;
    public AlienCongelado_Ataque(MoverEnemigos _m_e):base("AlienCongeladoAtaque",_m_e)
    {
        m_e = (MoverEnemigos)_m_e;
    }

    Rigidbody rb;
    float cont;


    public override void Inicio()
    {
        base.Inicio();
        rb = m_e.GetComponent<Rigidbody>();
        m_e.tag = "EnemigoBala";

    }
    public override void Actualizar()
    {
        base.Actualizar();  
        m_e.PuedeMorir = true;
        SaltoHaciaElJugador();
        return;

    }
    public override void Terminar()
    {
        base.Terminar();
    }

    void SaltoHaciaElJugador()
    {
        cont += Time.deltaTime;

        
        if (cont >=1)
        {
            m_e.cambiarestadoanimacion("AlienCAtacando");
            m_e.transform.Translate(1 * Vector3.forward);
           
            
        }else
        { 
            m_e.transform.LookAt(m_e._Jugador.transform);

        }
    }
}
