using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AlienCongelado_Correr : EnemigoEstadoBase
{
    MoverEnemigos m_e;
    public AlienCongelado_Correr(MoverEnemigos _m_e):base("AlienCongeladoCorrer",_m_e)
    {
        m_e = (MoverEnemigos)_m_e;
    }
    public float amplitudey = 2f; // Amplitud del movimiento en arcoíris
    public float amplitudex = 13f;
    public float frequency = 3f; // Frecuencia del movimiento en arcoíris
    public float velocidad = 20f;
    public Vector3 nuevapos;
    public float PosIn;
    

    public override void Inicio()
    {
        base.Inicio();
        amplitudex = Random.Range(8f,13f);
        frequency = Random.Range(2f,3f);

        

    }
    public override void Actualizar()
    {
        base.Actualizar();  
        m_e.PuedeMorir = true;
        if (m_e.EstaCercaDelJugador())
        {
            m_e.CambiarEstado(m_e._alienCongelado_Ataque);
            return;
        }
        if (m_e._llegoAlPunto)
        {
            MovimientoZicZac();
            return;
        }
        if (!(m_e._llegoAlPunto))
        {
            m_e.CambiarEstado(m_e._Idle);
            return;
        }
 
        

    }
    public override void Terminar()
    {
        base.Terminar();
    }


    void MovimientoZicZac()
    {
        
        float time = Time.time;
        float xOffset = Mathf.Sin(time * frequency) * amplitudex;
        nuevapos = new Vector3((PosIn + xOffset), -2, m_e.transform.position.z - 20f * Time.deltaTime);
        
        m_e.GetComponent<Rigidbody>().MovePosition(nuevapos);
        
        Vector3 direction = new Vector3(xOffset, 0f, -20f * Time.deltaTime);
        Quaternion rotation = Quaternion.LookRotation(direction, Vector3.up);
        m_e.transform.rotation = rotation;
    
    }
}
