using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JefeLvl1 : MonoBehaviour
{
    public float amplitudey = 2f; // Amplitud del movimiento en arco�ris
    public float amplitudex = 13f;
    public float frequency = 3f; // Frecuencia del movimiento en arco�ris
    public float velocidad = 20f;
    private Vector3 nuevapos;
    private float PosIn;
    private float direccion;
    // Start is called before the first frame update
    void Start()
    {
        direccion = 20;
    }

    // Update is called once per frame
    void Update()
    {
        float FILLBOSS = GetComponent<Jefe>().JefeVida / (float)20;
        GetComponent<Jefe>().Canvas.GetComponent<UI>().BossLife.value = FILLBOSS;
        if (Vector3.Distance(this.gameObject.transform.position, GetComponent<Jefe>()._Jugador.transform.position) < 20)
        {
            GetComponent<Jefe>().PuedeDisparar = false;
            GetComponent<CapsuleCollider>().enabled = false;
            Quaternion rotacion = Quaternion.Euler(0, 0, 0);
            transform.rotation = rotacion;
            direccion = -20;
            
        }
        if (transform.position.z >= 150f)
        {
            GetComponent<Jefe>().PuedeDisparar = true;
            GetComponent<CapsuleCollider>().enabled = true;
            Quaternion rotacion = Quaternion.Euler(0, 180, 0);
            transform.rotation = rotacion;
            direccion = 20; 
        }
        MovimientoZicZac();
    }

    void MovimientoZicZac()
    {

        float time = Time.time;
        float xOffset = Mathf.Sin(time * frequency) * amplitudex;
        nuevapos = new Vector3((PosIn + xOffset), -2, transform.position.z - direccion * Time.deltaTime);

        GetComponent<Rigidbody>().MovePosition(nuevapos);

        Vector3 direction = new Vector3(xOffset, 0f, -20f * Time.deltaTime);

    }
}
